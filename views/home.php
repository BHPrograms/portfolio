<div class="uk-container" style="margin-top: 2%;">
    <h1 class="uk-text-center">ASC Projects</h1>
    <div class="uk-flex uk-flex-center">
        <div class=" uk-width-1-2">
            <p class="uk-text-center black-text">While pursuing a degree in Applications Software Development at Alfred State College, several projects were created that are showcased here.</p>
        </div>
    </div>

    <div class="uk-flex uk-flex-center">
        <div class="uk-position-relative uk-visible-toggle uk-width-1-2@l tabindex=-1" uk-slider="center: true ">
            <ul class="uk-slider-items uk-container" style="padding: 2%;">
                <?php
                include_once("model/php/projects.php");
                $projects = retrieveProjects();
                foreach($projects as $project) {
                    include("views/project_slide.php");
                }
            ?>
            </ul>
            <div class="uk-flex uk-flex-center">
                <ul class="uk-slider-nav uk-dotnav"></ul>
            </div>
            <a class="white-text transparent-black uk-position-center-left uk-position-small uk-hidden-hover" href="#"
                uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="white-text transparent-black uk-position-center-right uk-position-small uk-hidden-hover" href="#"
                uk-slidenav-next uk-slider-item="next"></a>
        </div>
    </div>
</div>