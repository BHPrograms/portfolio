<li class="uk-text-center uk-width-2-3">
    <a href="Projects/<?php echo $project["Name"]; ?>/index.php">
        <div class="uk-flex uk-flex-center uk-flex-middle uk-height-small uk-padding-small">
            <img src="<?php echo $project["Thumbnail"]; ?>" style="width: 128px; height: 128;">
        </div>
        <h2 class="uk-text-center black-text" style="margin: 0;"><?php echo $project["Name"]; ?></h2>
        <div style="padding: 5px;">
            <p class="uk-text-center black-text"><?php echo $project["Tagline"];?></p>
        </div>
    </a>
</li>