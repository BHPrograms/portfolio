<!DOCTYPE html>
<html>

<head>
    <!--JQuery CDN-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

    <!--JQuery Validator CDN-->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js"></script>

    <!--General color palette-->
    <link href="css/palette.css" rel="stylesheet">

    <!--Fixes small issues caused otherwhere-->
    <link href="css/helpers.css" rel="stylesheet">
    <meta charset="utf-8" />
</head>


<body>
    <?php session_start(); ?>
        <nav class="uk-navbar-container white uk-box-shadow-small" style="padding-right: 2% !important;" uk-navbar>
            <a class="uk-navbar-item uk-logo" href="index.php"><img src="img/icons/abstract-logo.svg"
                    style="width: 64px; margin: 5px;">BHPrograms: ASC Portfolio</a>
            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav">
                </ul>
            </div>
        </nav>
        <div class="uk-flex uk-flex-stretch" uk-height-viewport="expand: true">