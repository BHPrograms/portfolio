
        </div>
        <!--Basic footer-->
        <footer class="navy box-shadow-top">
            <div class="uk-flex uk-flex-left" style="padding: 10px;">
                <div class="">
                    <h3 class="">BHPrograms</h3>
                    <ul class="uk-list">
                        <li><a class="black-text" href="https://www.bhprogramsdevelopment.com">BHProgramsDevelopment.com</a></li>
                        <li class="black-text"><a class="black-text" href="tel:6073013986">Phone: 607-301-3986</a></li>
                        <li class="black-text"><a class="black-text" href="mailto:info@bhprogramsdevelopment.com">Email: Info@BHProgramsDevelopment.com</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-text-center">
                <span>&copy; 2020 BHPrograms. All rights reserved.</span>
            </div>
        </footer>
    </body>
</html>