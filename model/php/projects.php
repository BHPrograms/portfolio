<?php
    require_once("DatabaseConfig.php");

    function retrieveProjects() {
        global $conn;

        $stmt = $conn->prepare('SELECT * FROM ASCProject');

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $projects = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $projects;
        } else {
            return false;
        }
    }
?>