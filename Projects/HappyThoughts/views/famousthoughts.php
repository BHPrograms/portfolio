<div class="uk-container">
    <h1 class="uk-text-center">Thought Leaders</h1>
    <p class="uk-text-center">
        Why not take your inspiraction from rich people?
        <br>  
        <a href="../legacycelebrities.php" class="uk-text-center" style="font-size: x-small;">Legacy Version</a>
    </p>
  


    <div uk-slider="center: true">

        <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
            <ul class="uk-slider-items uk-child-width-1-2@s uk-grid">
                <?php
                    $thoughts = simplexml_load_file("xml/FamousPeople.xml");
                    foreach($thoughts->Thought as $thought) {                    
                        //Variables that change between each slide
                        $thumbnail = $thought->Thumbnail;
                        $name = $thought->Celebrity;
                        $quote = $thought->Quote;
                                                                    
                        echo '<li style="width:500px;">
                                <div class="uk-card uk-card-default">
                                    <div class="uk-card-media-top">
                                        <img src="' . $thumbnail . '" style="height: 500px;"">
                                    </div>
                                    <div class="uk-card-body" style="height: 100px;">
                                        <h3 class="uk-card-title">' . $name .'</h3>
                                        <p>'. $quote .'</p>
                                    </div>
                                </div>
                            </li>';
                    }
                ?>
            </ul>

            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>  
        <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
    </div>
</div>