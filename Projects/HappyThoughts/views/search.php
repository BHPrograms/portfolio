
<head>
    <script src="js/Cloud/jquery.tagcanvas.js"></script>
    <script src="controls/CloudCrud.js"></script>
</head>

<div class="uk-container">
    <h1 class="uk-text-center" style="margin: 2%;">Custom Cloud</h1>
    <p class="uk-text-center">All thoughts containing your search</p>
    <?php include("searchcloud.php"); ?>
    <div style="margin: 2%;">
      <h1 class="uk-text-center">Contribute</h1>
      <p class="uk-text-center loginReq">Share your own quote, thought, or comment.</p>
      <p class="uk-text-center logoutReq">Sign in to contribute to the community.</p>
      <form id="communitycloud_form" class="loginReq uk-align-center" style="width: 250px;">
        <textarea name="community_thought" class="uk-textarea"></textarea>
        <label style="display: none;"><input class="uk-checkbox" type="checkbox" name="community_private" checked value="1"> Private</label>
        <p id="communitycloud_error" class="error"></p>
        <button id="btn_cloudsubmit" class="uk-button uk-button-default uk-align-center" type="button">Submit</button>
      </form>
    </div>
    
</div>