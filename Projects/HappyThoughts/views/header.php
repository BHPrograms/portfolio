<!DOCTYPE html>
<html>

<head>
    <title>Happy Thoughts</title>
    <!--JQuery CDN-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js"></script>

    <link href="css/palette.css" rel="stylesheet">
    <link href="css/helpers.css" rel="stylesheet">

    <meta charset="utf-8" />
</head>


<body>
    <?php session_start(); ?>
    <div class="page-wrapper">
        <div>
        <nav class="uk-navbar-container navy uk-box-shadow-large" style="padding-right: 2% !important;" uk-navbar>

            <a class="uk-navbar-item uk-logo" href="index.php">Happy Thoughts</a>
            <div class="uk-navbar-right">
                <ul class="uk-navbar-nav">
                    <li><a href="index.php" class=" uk-box-shadow-hover-large">Home</a></li>
                </ul>

            </div>
        </nav>