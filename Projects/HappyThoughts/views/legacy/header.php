<!DOCTYPE html>
<html>

<head>
    <link href="../css/legacy.css" rel="stylesheet">
    <link href="../css/palette.css" rel="stylesheet">
    <link href="../css/helpers.css" rel="stylesheet">
    <meta charset="utf-8" />
</head>

<body>
    <div class="nav shadowed moss">
        <br>
        <img src="../img/icons/BobRossIcon.png" style="width: 32px;">
        <a class="uk-navbar-item uk-logo" href="../index.php"><h3 style="color: black;">Happy Thoughts<h3></a>

        <ul class="right-text">
                    <li><a style="color: black;" href="index.php">Home</a></li>
                    <li><a style="color: black;" href="../thoughtleaders.php">Thought Leaders</a></li>
        </ul>
    </div>