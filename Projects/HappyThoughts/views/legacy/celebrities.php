<!DOCTYPE html>

<head>
    <title>The College Experience</title>
</head>



<div class="container centered shadowed ">
    <xml id="Celebrities" src="../xml/FamousPeople.xml"></xml>

    <div>
        <h1 class="centered header white-text ">Thought Leaders</h1>
    </div>

    <div class="centered" style="margin-left: 15%; margin-right: 15%;">
        <p class="centered paragraph" style="text-align: center; margin: 2%;">Why not take your inspiraction from rich people?</p>
        <p class="centered paragraph" style="text-align: center; font-size: x-small;">IE9 Recommended</p>
    </div>

    <div style="padding-top: 2%;">
        <div class="moss" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%;">
            <span class="centered header white-text" datasrc="#Celebrities" datafld="College"></span>
        </div>

        <div>
            <img datasrc="#Celebrities" datafld="Thumbnail" style="width: 100%; height: auto;">
            <span class="paragraph" style="margin-left: 2%;" datasrc="#Celebrities"datafld="Celebrity"></span>
            <br>
            <span class="paragraph" style="margin-left: 2%;" datasrc="#Celebrities"datafld="Quote"></span>
        </div>

        <div>
            <button style="margin: 2%;" style="margin: 2%;" onclick="Celebrities.recordset.moveFirst()">
                |&lt; First
            </button>
            <button style="margin: 2%;"
                onclick="Celebrities.recordset.movePrevious() ; if(Celebrities.recordset.BOF) Celebrities.recordset.moveLast()">
                &nbsp;&nbsp;&lt; Back &nbsp;&nbsp;
            </button>
            <button style="margin: 2%;"
                onclick="Celebrities.recordset.moveNext() ; if(Celebrities.recordset.EOF) Celebrities.recordset.moveFirst()">
                Forward &gt;
            </button>
            <button style="margin: 2%;" onclick="Celebrities.recordset.moveLast()">
                Last &gt;|
            </button>
        </div>
    </div>
</div>
</div>