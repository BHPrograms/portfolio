<!DOCTYPE html>
<html>

<head>
    <link href="../css/legacy.css" rel="stylesheet">
    <link href="../css/palette.css" rel="stylesheet">
    <meta charset="utf-8" />
</head>

<body>
    <div class="nav box-shadow-top moss" style="margin-top: 5%; margin-bottom: 0px;">
            <br>
            <p class="paragraph">Happy Thoughts</p>
        
                <a style="color: black;" href="index.php">Home</a>
                <br>
                <a style="color: black;" href="../thoughtleaders.php">Thought Leaders</a>

            <hr>
            <p class="white-text center-text">Copyright 2020 BHPrograms</p>
    </div>