    <head>

        <!--JQuery Formatting upon Login/Logout-->
        <script src="../js/Formatting/statechange.js"></script>
        <script src="../js/Authentication/observer.js"></script>

    </head>
    </div>
    <!--Basic footer-->
    <footer class="navy box-shadow-top" style="margin-top: 2%;">
        <div class="uk-flex uk-flex-left">
            <div style="margin-right: 2%; margin-left: 2%;">
                <h3>Happy Thoughts</h3>
                <a style="color: black;" href="https://www.bhprogramsdevelopment.com">A BHPrograms Project</a>
            </div>
            <div style="margin-right: 2%; margin-left: 2%;">
                <h3>Collections</h3>
                <ul class="uk-list">
                <li><a style="color: black;" href="../index.php">Community Cloud</a></li>
                    <li class=""><a style="color: black;" href="../thoughtleaders.php">Thought Leaders</a></li>
                    <li><a style="color: black;" href="../mythoughts.php" class="loginReq">My Thoughts</a></li>
                </ul>
            </div>
        </div>
        <div class="uk-text-center">
            <span>&copy; 2020 BHPrograms. All rights reserved.</span>
        </div>
    </footer>
    </body>
    
    <?php
        if (isset($_SESSION["loggedIn"])) {
            echo '<script type="text/javascript">
                    $(document).ready(function() {
                        formatAuth('. $_SESSION["loggedIn"] .');
                    });
                </script>';
        } else {
            echo '<script type="text/javascript">
                    $(document).ready(function() {
                        formatAuth(false);
                    });
                  </script>';
        }   
    ?>

    </html>