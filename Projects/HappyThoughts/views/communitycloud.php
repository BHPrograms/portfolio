<head>
    <script src="controls/ThoughtCloud.js"></script>
</head>

<div id="tagContainer">
    <canvas width="1200" height="1000" id="myCanvas" class="uk-align-center">
        <ul>
            <?php
                require_once("model/php/DatabaseConfig.php");
                global $conn;

                $sql = 'SELECT thought
                FROM happythought
                WHERE Private = 1
                ;';

                
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach($rows as $row) {
                    echo '<li><a>' . $row['thought'] .'</a></li>';
                }
            ?>
        </ul>
    </canvas>
</div>