
<head>
    <script src="js/Cloud/jquery.tagcanvas.js"></script>
    <script src="controls/CloudCrud.js"></script>
</head>

<div class="uk-container">
    <h1 class="uk-text-center" style="margin: 2%;">My Thoughts</h1>
    <p class="uk-text-center logoutReq">Sign in to view your collection.</p>
    <?php include("usercloud.php"); ?>
    <div style="margin: 2%;" class="loginReq">
      <h1 class="uk-text-center">Expand</h1>
      <p class="uk-text-center">Expand your own cloud.</p>
      <form id="communitycloud_form" class="loginReq uk-align-center" style="width: 250px;">
        <textarea name="community_thought" class="uk-textarea"></textarea>
        <label><input class="uk-checkbox" type="checkbox" name="community_private" value="1"> Private</label>
        <p id="communitycloud_error" class="error"></p>
        <button id="btn_cloudsubmit" class="uk-button uk-button-default uk-align-center" type="button">Submit</button>
      </form>
    </div>
    
</div>