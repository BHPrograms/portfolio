<form id="signup_form">
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <input id="signup_user" name="signup_user" class="uk-input bottom" placeholder="Username">
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <input id="signup_pass" name="signup_pass" class="uk-input" type="password" placeholder="Password">
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <span class="uk-form-icon" uk-icon="icon: sign-in"></span>
            <button id="btn_signup" name="btn_signup" class="uk-button uk-text-capitalize" type="button">Sign Up</button>
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <p id="signup_error" class="error"></p>
    </div>
</form>

<!--Validate in .validate-->
<script type="text/javascript">
$.validator.setDefaults({
    success: "valid"
});

//Custom password verification
$.validator.addMethod("signup_pass", function(value, element) {
    return this.optional(element) || new RegExp("^([a-zA-Z0-9@*#]{8,24})").test(value);
}, "Passwords must be 8-24 characters long and consist only of letters, numbers, or the characters @*#");


//Set form rules, check validity
$("#signup_form").validate({
    rules: {
        signup_user: {
            required: true,
            email: true
        },
        signup_pass: "required signup_pass",
        signup_terms: {
            required: true
        }
    }
});
</script>