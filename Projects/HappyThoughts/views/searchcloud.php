<head>
    <link href="../css/ThoughtCloud.css" rel="stylesheet">
    <script src="../controls/ThoughtCloud.js"></script>
</head>

<div id="tagContainer">
    <canvas width="1200" height="1000" id="myCanvas" class="uk-align-center">
        <ul>
            <?php
                if (isset($_GET["q"])) {
                    require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");

                    $sql = 'SELECT ThoughtID, Author, Thought
                    FROM HappyThought
                    WHERE Private = 1
                    ;';

                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

                    $query = $_GET["q"];
                    $results= 0;
                    foreach($rows as $row) {
                        if (strpos(strtoupper($row['Thought']), strtoupper($query)) !== false) {
                            echo '<li><a href="javascript:void(0);" onclick="displayModal('. $row['ThoughtID'] . ')">' . $row['Thought'] .'</a></li>';
                            $results++;
                        }
                    }

                    if ($results == 0) {
                        echo '<li><a href="#">No matching thoughts found</a></li>';
                    }
                } else {
                    echo '<li><a href="#">No matching thoughts found</a></li>';
                }
                
            ?>
        </ul>
    </canvas>
</div>