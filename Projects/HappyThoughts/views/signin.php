<form id="signin_form">
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <input id="signin_user" name="signin_user" class="uk-input bottom" placeholder="Username">
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <input id="signin_pass" name="signin_pass" class="uk-input" type="password" placeholder="Password">
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <div class="uk-inline">
            <span class="uk-form-icon" uk-icon="icon: sign-in"></span>
            <button id="btn_signin" name="btn_signin" class="uk-button uk-text-capitalize" type='button'>Sign In</button>
        </div>
    </div>
    <div style="display: block; margin-bottom: 5px;">
        <p id="signin_error" class="error"></p>
    </div>
</form>

<!--Validate in .validate-->
<script type="text/javascript">
$.validator.setDefaults({
    success: "valid"
});

//Custom password verification
$.validator.addMethod("signin_pass", function(value, element) {
    return this.optional(element) || new RegExp("^([a-zA-Z0-9@*#]{8,24})").test(value);
}, "Passwords must be 8-24 characters long and consist only of letters, numbers, or the characters @*#");


//Set form rules, check validity
$("#signin_form").validate({
    rules: {
        signin_user: {
            required: true,
            email: true
        },
        signin_pass: "required signin_pass"
    }
});
</script>