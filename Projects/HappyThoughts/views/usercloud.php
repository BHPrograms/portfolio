<head>
    <link href="../css/ThoughtCloud.css" rel="stylesheet">
    <script src="../controls/ThoughtCloud.js"></script>
</head>

<div id="tagContainer">
    <canvas width="1200" height="1000" id="myCanvas" class="logininReq">
        <ul>
            <?php
                require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");

                if (isset($_SESSION["user"])) {
                    $user = $_SESSION["user"];

                    $stmt = $conn->prepare('SELECT ThoughtID, Author, Thought FROM HappyThought WHERE Author = :author');
                    $stmt->bindParam(':author', $user);
                    $stmt->execute();

                    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    if ($stmt->rowCount() != 0) {
                        
                        foreach($rows as $row) {
                            echo '<li><a href="javascript:void(0);" onclick="displayModal('. $row['ThoughtID'] . ')">' . $row['Thought'] .'</a></li>';
                        }
                    } else {
                        echo '<li><a href="#">No thoughts found</a></li>';
                    }
                }
            ?>
        </ul>
    </canvas>

    <?php 
        if (isset($_SESSION["user"]) && $stmt->rowCount() != 0) {
            foreach($rows as $row) {
                if ($row['Author'] != $_SESSION["user"]) {
                    echo '<div id="modal' . $row['ThoughtID'] . '" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <h2 class="uk-modal-title">' . $row['Author'] . '</h2>
                                <p>' . $row['Thought'] . '</p>
                                <p class="uk-text-center">
                                    <button class="uk-button uk-button-default uk-modal-close" type="button">Close</button>
                                </p>
                            </div>
                        </div>';
                } else {
                    echo '<div id="modal' . $row['ThoughtID'] . '" uk-modal>
                        <div class="uk-modal-dialog uk-modal-body">
                            <h2 class="uk-modal-title">' . $row['Author'] . '</h2>
                            <p>' . $row['Thought'] . '</p>
                            <div class="uk-align-center">
                                <a class="user_edit" value="' . $row['ThoughtID'] .'"uk-icon="pencil" style="margin: 10px;"></a>
                                <div uk-dropdown="mode: click">
                                    <form id="communityupdate_form" class="loginReq uk-align-center" style="width: 250px;">
                                        <textarea name="community_thought" class="uk-textarea"></textarea>
                                        <p id="communitycloud_error" class="error"></p>
                                        <input style="display: none;" name="ThoughtID" value="' . $row['ThoughtID'] . '">
                                        <button id="btn_cloudupdate" class="uk-button uk-button-default uk-align-center" type="button">Update</button>
                                    </form>
                                </div>
                                <a  class="user_delete" onclick="deleteThought(' . $row['ThoughtID'] .');" uk-icon="trash" style="margin: 10px;"></a>
                            </div>
                            <p class="uk-text-center">
                                <button class="uk-button uk-button-default uk-modal-close" type="button">Close</button>
                            </p>
                        </div>
                    </div>';
                }
            }
        }
    ?>
</div>