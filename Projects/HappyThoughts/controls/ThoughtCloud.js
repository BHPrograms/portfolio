
var options = {
    textColour: "#000000",
    textHeight: 20,
    depth: 0.99,
};

/* Creates canvas using passed options, hides in case of error */
window.onload = function() {
    try {
        TagCanvas.Start('myCanvas', '', options);
    } catch (e) {
        document.getElementById('tagContainer').style.display = 'none';
        console.log(e.message);
    }
};
