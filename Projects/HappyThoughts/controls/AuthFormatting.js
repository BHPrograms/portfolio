//Hides or shows appropriate classes
function formatAuth(loginStatus) {
    if (loginStatus) {
        $(".loginReq").show();
        $(".logoutReq").hide();
    } else {
        $(".loginReq").hide();
        $(".logoutReq").show();
    }
}