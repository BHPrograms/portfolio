
$(document).ready(function() {
    //Default theme is light. If "night" is stored, toggles
    switchThemes(readCookie("theme"));

    //Toggles theme and specifies light was selected
    $('#btn_light').bind('click', function() {
        switchThemes("light");
        location.reload();
    });

    //Toggles theme and specifies light was selected
    $('#btn_dark').bind('click', function() {
        switchThemes("dark");
        location.reload();
    });

    //Toggles theme and specifies light was selected
    $('#btn_funky').bind('click', function() {
        switchThemes("funky");
        location.reload();
    });

    /* Cloud size formatting */
    $('#btn_cloud_small').bind('click', function() {
        createCookie("width", 500, 365);
        createCookie("height", 500, 365);
        location.reload();
    });

    $('#btn_cloud_medium').bind('click', function() {
        createCookie("width", 850, 365);
        createCookie("height", 700, 365);
        location.reload();
    });

    $('#btn_cloud_large').bind('click', function() {
        createCookie("width", 1200, 365);
        createCookie("height", 1000, 365);
        location.reload();
    });

    /* Cloud text size formatting */
    $('#btn_text_small').bind('click', function() {
        createCookie("textSize", 10, 365);
        location.reload();
    });

    $('#btn_text_medium').bind('click', function() {
        createCookie("textSize", 15, 365);
        location.reload();
    });

    $('#btn_text_large').bind('click', function() {
        createCookie("textSize", 20, 365);
        location.reload();
    });

    /*Sets slide transition to timeout length, increments*/
    function switchThemes(theme) {
        //Sets saved size
        $("canvas").width(readCookie("width"));
        $("canvas").height(readCookie("height"));

        //Sets theme cookie to last selected theme
        createCookie("theme", theme, 365);

        /* Changes to specified theme color */
        var textColor;
        var backgroundColor;
        if (theme == "dark") {
            textColor = "white";
            backgroundColor = "black";
        } else if (theme == "light") {
            textColor = "black";
            backgroundColor = "white";
        } else if (theme == "funky") {
            textColor = "white";
            backgroundColor = "#3c1f41";
        }

        /* Sets all affected elements */
        $("p").css("color", textColor);
        $("a").css("color", textColor);
        $("h1").css("color", textColor);
        $("h3").css("color", textColor);
        $("span").css("color", textColor);
        $("button").css("color", textColor);
        $("div").css("background-color", backgroundColor);
        $("nav").css("background-color", backgroundColor);
        $(".uk-modal").css("background", "none");
    }
});