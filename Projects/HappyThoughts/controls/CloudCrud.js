//Handles every CRUD action
function updateThought(formData) {
    $.ajax({
        url: 'model/php/UpdateThoughts.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
            if (data == "Please sign in to continue.") {
                UIkit.notification(data, { status: 'danger' });
            } else if (data == "Thought shared successfully.") {
                UIkit.notification(data, { status: 'success' });
                setTimeout(() => { location.reload(); }, 2000);
            } else if (data == "Thought could not be shared at this time.") {
                UIkit.notification(data, { status: 'danger' });
            } else if (data == "Thought deleted successfully.") {
                UIkit.notification(data, { status: 'success' });
                setTimeout(() => { location.reload(); }, 2000);
            } else if (data == "Thought could not be deleted at this time.") {
                UIkit.notification(data, { status: 'danger' });
            } else if (data == "Thought updated successfully.") {
                UIkit.notification(data, { status: 'success' });
                setTimeout(() => { location.reload(); }, 2000);
            } else if (data == "Thought could not be updated at this time.") {
                UIkit.notification(data, { status: 'danger' });
            }
            console.log(data);
        }
    });
}

//Retrieves information from forms and passes
$(document).ready(function() {
    $("#btn_cloudsubmit").click(function() {
        var form = $('#communitycloud_form')[0];
        var formData = new FormData(form);
        formData.append("action", "add");
        updateThought(formData);
    });

    $("#btn_cloudupdate").click(function() {
        var form = $('#communityupdate_form')[0];
        var formData = new FormData(form);
        formData.append("action", "update");
        updateThought(formData);
    });
});

//Delete is not done through a form, but form data is created
function deleteThought(ID) {
    var formData = new FormData();
    formData.append("ThoughtID", ID);
    formData.append("action", "delete");
    updateThought(formData);
}