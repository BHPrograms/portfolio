<?php


/*Manages creation, edit, and deletion of thoughts. Sanitizes data
and passes in auth info to prevent client side edits of IDs etc */
class ThoughtModel {
    public function add() {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");
        
        $thought = filter_input(INPUT_POST, 'community_thought', FILTER_SANITIZE_STRING);
        $user = $_SESSION["user"];
        $private = 0;

        if (isset( $_POST["community_private"])) {
            $private = 1;
        }
        
    
    
        $stmt = $conn->prepare("INSERT INTO happythought(Author, Thought, Private) VALUES(:author, :thought, :private);");
        $stmt->bindParam(':author', $user);
        $stmt->bindParam(':thought', $thought);
        $stmt->bindParam(':private', $private);

        if ($stmt->execute()) {
            echo "Thought shared successfully.";
        } else {
            echo "Thought could not be shared at this time.";
        }
    }
    
    public function delete() {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");
        
        $thoughtID = $_POST['ThoughtID'];
        $user = $_SESSION["user"];
    
        $stmt = $conn->prepare("DELETE FROM happythought WHERE Author = :author AND ThoughtID = :thoughtID");
        $stmt->bindParam(':author', $user);
        $stmt->bindParam(':thoughtID', $thoughtID);

        if ($stmt->execute()) {
            echo "Thought deleted successfully.";
        } else {
            echo "Thought could not be deleted at this time.";
        }
    }
    
    public function update() {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");
        
        $thought = filter_input(INPUT_POST, 'community_thought', FILTER_SANITIZE_STRING);
        $user = $_SESSION["user"];
        $thoughtID = $_POST['ThoughtID'];
    
        $stmt = $conn->prepare("UPDATE happythought SET Thought = :thought WHERE Author = :author AND ThoughtID = :thoughtID");
        $stmt->bindParam(':author', $user);
        $stmt->bindParam(':thought', $thought);
        $stmt->bindParam(':thoughtID', $thoughtID);

        if ($stmt->execute()) {
            echo "Thought updated successfully.";
        } else {
            echo "Thought could not be updated at this time.";
        }
    }
}

if ($_SERVER['REQUEST_METHOD']=='POST') { 
    session_start();
    
    if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {
        $model = new ThoughtModel;
    
        if(isset($_POST['action'])){
            if ($_POST['action'] == "add") {
                $model->add($_POST);
            } else if ($_POST['action'] == "delete") {
                $model->delete($_POST);
            } else if ($_POST['action'] == "update") {
                $model->update($_POST);
            }
        }
    } else {
        echo "Please sign in to continue.";
        exit;
    }
}

?>