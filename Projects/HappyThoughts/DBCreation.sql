-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 10, 2020 at 03:35 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `happythoughts`
--

USE `bhprogra_portfolio`;

-- --------------------------------------------------------

--
-- Table structure for table `happythought`
--

DROP TABLE IF EXISTS `happythought`;
CREATE TABLE IF NOT EXISTS `happythought` (
  `ThoughtID` int(11) NOT NULL AUTO_INCREMENT,
  `Author` varchar(200) DEFAULT NULL,
  `Thought` varchar(250) DEFAULT NULL,
  `Private` tinyint(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (`ThoughtID`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `happythought`
--

INSERT INTO `happythought` (`ThoughtID`, `Author`, `Thought`, `Private`) VALUES
(1, 'Bob@gmail.com', '\"Don\'t worry, be happy\" - Bobby McFerrin', 1),
(2, 'Bob@gmail.com', '\"Imagine How is Touch The Sky\" - Reddit', 1),
(3, 'Bob@gmail.com', '\"Every time someone steps up and says who they are, the world becomes a better, more interesting place.\" - Raymond Holt', 1),
(4, 'Bob@gmail.com', '\"We don\'t make mistakes, just happy little accidents.\" - Bob Ross', 1),
(5, 'Bob@gmail.com', '\"The greatest glory in living lies not in never falling, but in rising every time we fall.\" -Nelson Mandela', 1),
(6, 'Bob@gmail.com', '\"If nothing matters, and everything is pointless, may as well just...fucken be happy anyway.\" -Exurbia', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(200) NOT NULL DEFAULT '0',
  `PassHash` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserID`, `Username`, `PassHash`) VALUES
(1, 'Bob@gmail.com', '$2y$10$Rrg0sMyHMi4ra.vlLOjsfgqIse2bdqbfssssfggAShzTXn/TUpH91.BjI2m4zmQW');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
