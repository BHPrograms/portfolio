Limitations
    - Time was a big limitation. Unfortunately, things have been hectic. This is part of why I chose a minimal design.
    - IE: As always, a legacy view was created for IE, but the formatting is limited.
    - TagCloud: Script used for cloud has several issues, which resulted in interesting solutions. There are no
        event listeners or callbacks that can be set as options. All that can be passed in the tag is the text
        and an onClick handler; uk-toggle, or other attributes, can not be set.
    - Formatting: Because of the very minimal design, there isn't a whole lot for the user to format. This is
        likely due to creating the interface and then trying to create formatting options. This should have
        been planned out in advance, so elements could be created that worked well with formatting.


Design
    - A very minimalistic design was chosen. This is largely to cater to the aesthetic of the tag cloud, and
        because I believe it matched the theme well. I have not tried many minimal designs before, and thus
        likely over-simplified. I apologize if the website is more basic than what you had in mind; I promise
        I know how to make tables etc.


Explanation
    -Theming: Theme options are stored in tookies, and managed by Theming.js and ThoughtCloud.js
    -Privacy: Thoughts created in community are public. Thoughts created under MyThoughts can be
        shown only to the user, or publicly displayed.
    -DB: A db with data has been been created and attached. For MyThoughts; You will need to create
        an account and create your own thoughts.