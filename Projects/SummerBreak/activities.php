<?php
    include('model/php/Activities.php');
    require_once 'vendor/autoload.php';
    
    $loader = new \Twig\Loader\FilesystemLoader('views');
    $twig = new \Twig\Environment($loader, [
        'cache' => 'twig_cache',
    ]);

    $loginStatus = false;
    if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {
        $loginStatus = true;
    }

    $username = false;
    if (isset($_SESSION["user"])) {
        $username = $_SESSION["user"];
    }
    

    echo $twig->render('activity.twig', ["activities" => getActivities(), "loginStatus" => $loginStatus, "username" => $username]);
?>