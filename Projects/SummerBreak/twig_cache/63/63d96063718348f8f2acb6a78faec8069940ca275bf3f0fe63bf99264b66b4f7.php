<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.twig */
class __TwigTemplate_58d5585ce8741b2a83eea2bc03720c98ebe477380bb41a51b12553e53573eb50 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base_view.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("base_view.twig", "home.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div>
        <div class=\"uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle\"
            style=\"background-image: url(img/pool.jpg);\" uk-height-viewport>
            <h2 class=\"uk-heading-large\" style=\"color: white;\">Get your Summer on</h2>
        </div>

        <div class=\"uk-flex uk-flex-between uk-flex-middle\">
            <div class=\"uk-text-center uk-width-2-3\">
                <h2 class=\"uk-heading-medium\">Summer Fun</h2>
                <div style=\"width: 50%; margin: 0 auto;\">
                    <p class=\"uk-text-lead\">With Summer right around the corner, there's a lot of time to waste. Why not
                        waste
                        it in the best of ways, and check out some of these Summer fun activities? From staying at home, to
                        essential shopping, to staying at home, to looking out the window, or even staying home - there's a
                        lot
                        to be done! Take our lead and make the most of it.</p>
                </div>
            </div>
            <div class=\"uk-width-1-3\"><a href=\"activities.php\"><img src=\"img/icecream.jpg\" style=\"height: 100%; width: 100%;\"></a></div>
        </div>

        <div class=\"uk-flex uk-flex-between uk-flex-middle\">
            <div class=\"uk-width-1-3\"><a href=\"jokes.php\"><img src=\"img/pineapple.jpg\" style=\"height: 100%; width: 100%;\"></a></div>
            <div class=\"uk-text-center uk-width-2-3\">
                <h2 class=\"uk-heading-medium\">Relax with some jokes</h2>
                <div style=\"width: 50%; margin: 0 auto;\">
                    <p class=\"uk-text-lead\">School is harsh, and it can be hard to destress. Why not get started with some
                        humor? Get your laugh on, or see if you can get others to.</p>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "home.twig", "C:\\Users\\brand\\Documents\\GitHub\\Portfolio\\Projects\\SummerBreak\\views\\home.twig");
    }
}
