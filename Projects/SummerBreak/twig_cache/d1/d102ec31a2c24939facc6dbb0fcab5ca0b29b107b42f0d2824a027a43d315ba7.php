<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base_view.twig */
class __TwigTemplate_d54d08e5bcd9d1cddb93e105dd022bc08f031353dff09d6207cfc705df6c68cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 23
        echo "    </head>

    <?php session_start(); ?>
    <body>
        <div id=\"header\">
            ";
        // line 28
        $this->displayBlock('header', $context, $blocks);
        // line 40
        echo "        </div>

        <div id=\"content\" uk-height-viewport=\"expand: true\">";
        // line 42
        $this->displayBlock('content', $context, $blocks);
        echo "</div>

        <div id=\"footer\">
            ";
        // line 45
        $this->displayBlock('footer', $context, $blocks);
        // line 65
        echo "        </div>
    </body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "            <title>Summer Break</title>
            <!--JQuery CDN-->
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

            <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
                integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\" crossorigin=\"anonymous\"></script>

            <!--JQuery Validator CDN-->
            <script src=\"https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js\"></script>
            <script src=\"https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js\"></script>

            <!-- UIkit CSS -->
            <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css\" />

            <!-- UIkit JS -->
            <script src=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js\"></script>
            <script src=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js\"></script>
        ";
    }

    // line 28
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "                <nav class=\"uk-navbar-container navy uk-box-shadow-large\" style=\"padding-right: 2% !important;\" uk-navbar>
                    <a class=\"uk-navbar-item uk-logo\" href=\"index.php\">Summer Break</a>
                    <div class=\"uk-navbar-right\">
                        <ul class=\"uk-navbar-nav\">
                            <li><a href=\"index.php\" class=\"uk-box-shadow-hover-large\">Home</a></li>
                            <li><a href=\"activities.php\" class=\"uk-box-shadow-hover-large\">Activities</a></li>
                            <li><a href=\"jokes.php\" class=\"uk-box-shadow-hover-large\">Jokes</a></li>
                        </ul>
                    </div>
                </nav>
            ";
    }

    // line 42
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 45
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "               <footer class=\"navy box-shadow-top\">
                    <hr>
                    <div class=\"uk-flex uk-flex-around\">
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3>Summer Break</h3>
                            <a style=\"color: black;\" href=\"https://www.bhprogramsdevelopment.com\">A BHPrograms Project</a>
                        </div>
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3><a style=\"color: black;\" href=\"activities.php\">Activities</a></h3>
                        </div>
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3><a style=\"color: black;\" href=\"jokes.php\">Jokes</a></h3>
                        </div>
                    </div>
                    <div class=\"uk-text-center\">
                        <span>&copy; 2020 BHPrograms. All rights reserved.</span>
                    </div>
                </footer>
            ";
    }

    public function getTemplateName()
    {
        return "base_view.twig";
    }

    public function getDebugInfo()
    {
        return array (  128 => 46,  124 => 45,  118 => 42,  104 => 29,  100 => 28,  79 => 5,  75 => 4,  69 => 65,  67 => 45,  61 => 42,  57 => 40,  55 => 28,  48 => 23,  46 => 4,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base_view.twig", "C:\\Users\\brand\\Documents\\GitHub\\Portfolio\\Projects\\SummerBreak\\views\\base_view.twig");
    }
}
