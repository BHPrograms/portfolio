<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* activity_slide.twig */
class __TwigTemplate_a770bd827a8cd58c19c861caf260a3c93eb87cf7c5227e1aeb005fad31468647 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo " <li class=\"uk-width-3-4\">
    <div class=\"uk-panel\">
        <img src=\"";
        // line 3
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["activity"] ?? null), "thumbnail", [], "any", false, false, false, 3), "html", null, true);
        echo "\" style=\"height: 100%; width: 100%;\">
        <div class=\"uk-position-center uk-panel\" style=\"width: 100%;\">
            <div class=\"uk-text-center\" style=\"width: 100%; background: rgba(0, 0, 0, 0.5);\">
                <h2>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["activity"] ?? null), "header", [], "any", false, false, false, 6), "html", null, true);
        echo "</h2>
                <p class=\"uk-text-lead\">";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["activity"] ?? null), "description", [], "any", false, false, false, 7), "html", null, true);
        echo "</p>
                <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["activity"] ?? null), "link", [], "any", false, false, false, 8), "html", null, true);
        echo "\">Find Out More</a>
            </div>
        </div>
    </div>
</li>";
    }

    public function getTemplateName()
    {
        return "activity_slide.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 8,  51 => 7,  47 => 6,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "activity_slide.twig", "D:\\Archive\\School\\Alfred State\\Sophmore Year\\Spring Semester\\Web Programming II\\Assignments\\GLOs\\Backups\\Glo003\\Glo003\\views\\activity_slide.twig");
    }
}
