<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* signin.php */
class __TwigTemplate_5de229685fdb121caa4ebcb4d6aa71d9a7de4ff73b799aafd6a495818543465c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<form id=\"signin_form\">
    <div style=\"display: block; margin-bottom: 5px;\">
        <div class=\"uk-inline\">
            <input id=\"signin_user\" name=\"signin_user\" class=\"uk-input bottom\" placeholder=\"Username\">
        </div>
    </div>
    <div style=\"display: block; margin-bottom: 5px;\">
        <div class=\"uk-inline\">
            <input id=\"signin_pass\" name=\"signin_pass\" class=\"uk-input\" type=\"password\" placeholder=\"Password\">
        </div>
    </div>
    <div style=\"display: block; margin-bottom: 5px;\">
        <div class=\"uk-inline\">
            <span class=\"uk-form-icon\" uk-icon=\"icon: sign-in\"></span>
            <button id=\"btn_signin\" name=\"btn_signin\" class=\"uk-button uk-text-capitalize\" type='button'>Sign In</button>
        </div>
    </div>
    <div style=\"display: block; margin-bottom: 5px;\">
        <p id=\"signin_error\" class=\"error\"></p>
    </div>
</form>

<!--Validate in .validate-->
<script type=\"text/javascript\">
\$.validator.setDefaults({
    success: \"valid\"
});

//Custom password verification
\$.validator.addMethod(\"signin_pass\", function(value, element) {
    return this.optional(element) || new RegExp(\"^([a-zA-Z0-9@*#]{8,24})\").test(value);
}, \"Passwords must be 8-24 characters long and consist only of letters, numbers, or the characters @*#\");


//Set form rules, check validity
\$(\"#signin_form\").validate({
    rules: {
        signin_user: {
            required: true,
            email: true
        },
        signin_pass: \"required signin_pass\"
    }
});
</script>";
    }

    public function getTemplateName()
    {
        return "signin.php";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "signin.php", "D:\\Archive\\School\\Alfred State\\Sophmore Year\\Spring Semester\\Web Programming II\\Assignments\\GLOs\\Backups\\Glo003\\Glo003\\views\\signin.php");
    }
}
