<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base_view.twig */
class __TwigTemplate_ba2fab6bff99eeb78e23466866b7a9dfc2465ad2f0c12bbd13ed17e5f37b2bc0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 25
        echo "    </head>

    <?php session_start(); ?>
    <body>
        <div id=\"header\">
            ";
        // line 30
        $this->displayBlock('header', $context, $blocks);
        // line 70
        echo "        </div>

        <div id=\"content\" uk-height-viewport=\"expand: true\">";
        // line 72
        $this->displayBlock('content', $context, $blocks);
        echo "</div>

        <div id=\"footer\">
            ";
        // line 75
        $this->displayBlock('footer', $context, $blocks);
        // line 95
        echo "        </div>
    </body>
</html>";
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "            <!--JQuery CDN-->
            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

            <script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.min.js\"
                integrity=\"sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=\" crossorigin=\"anonymous\"></script>

            <!--JQuery Validator CDN-->
            <script src=\"https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.min.js\"></script>
            <script src=\"https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js\"></script>

            <!-- UIkit CSS -->
            <link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css\" />

            <!-- UIkit JS -->
            <script src=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js\"></script>
            <script src=\"https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js\"></script>

            <!-- Auth controls and formatting -->
            <script src=\"../controls/AuthControls.js\"></script>
        ";
    }

    // line 30
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "                <nav class=\"uk-navbar-container navy uk-box-shadow-large\" style=\"padding-right: 2% !important;\" uk-navbar>
                    <a class=\"uk-navbar-item uk-logo\" href=\"../index.php\">Summer Break</a>
                    <div class=\"uk-navbar-right\">
                        <ul class=\"uk-navbar-nav\">
                            <li><a href=\"jokes.php\" class=\"uk-box-shadow-hover-large\">Jokes</a></li>
                            <li><a href=\"activities.php\" class=\"uk-box-shadow-hover-large\">Activities</a></li>
                            <li><a href=\"index.php\" class=\"uk-box-shadow-hover-large\">Home</a></li>
                            ";
        // line 38
        if (0 === twig_compare(($context["loginStatus"] ?? null), false)) {
            // line 39
            echo "                                <li id=\"li_login\">
                                    <a href=\"index.php\" class=\"uk-box-shadow-hover-large logoutReq\">Login</a>
                                    <div id=\"login_dropdown\" uk-dropdown='mode: click;' class=\"uk-width-1-4\">
                                        <ul uk-accordion class=\"uk-width-1-1\">
                                            <li class=\"uk-open\">
                                                <a class=\"uk-accordion-title\" href=\"#\">Sign In</a>
                                                <div class=\"uk-accordion-content\">
                                                    ";
            // line 46
            $this->loadTemplate("signin.php", "base_view.twig", 46)->display($context);
            // line 47
            echo "                                                </div>
                                            </li>

                                            <li>
                                                <a class=\"uk-accordion-title\" href=\"#\">Sign Up</a>
                                                <div class=\"uk-accordion-content\">
                                                    ";
            // line 53
            $this->loadTemplate("signup.php", "base_view.twig", 53)->display($context);
            // line 54
            echo "                                                </div>

                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            ";
        }
        // line 61
        echo "                            ";
        if (0 === twig_compare(($context["loginStatus"] ?? null), true)) {
            // line 62
            echo "                                <li id=\"li_logout\"><a href=\"#\"
                                    class=\" uk-box-shadow-hover-large\">Log Out</a></li>
                            ";
        }
        // line 65
        echo "                        </ul>

                    </div>
                </nav>
            ";
    }

    // line 72
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 75
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 76
        echo "               <footer class=\"navy box-shadow-top\" style=\"margin-top: 2%;\">
                    <hr>
                    <div class=\"uk-flex uk-flex-around\">
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3>Summer Break</h3>
                            <a style=\"color: black;\" href=\"https://www.bhprogramsdevelopment.com\">A BHPrograms Project</a>
                        </div>
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3><a style=\"color: black;\" href=\"activities.php\">Activities</a></h3>
                        </div>
                        <div style=\"margin-right: 2%; margin-left: 2%;\">
                            <h3><a style=\"color: black;\" href=\"jokes.php\">Jokes</a></h3>
                        </div>
                    </div>
                    <div class=\"uk-text-center\">
                        <span>&copy; 2020 BHPrograms. All rights reserved.</span>
                    </div>
                </footer>
            ";
    }

    public function getTemplateName()
    {
        return "base_view.twig";
    }

    public function getDebugInfo()
    {
        return array (  173 => 76,  169 => 75,  163 => 72,  155 => 65,  150 => 62,  147 => 61,  138 => 54,  136 => 53,  128 => 47,  126 => 46,  117 => 39,  115 => 38,  106 => 31,  102 => 30,  79 => 5,  75 => 4,  69 => 95,  67 => 75,  61 => 72,  57 => 70,  55 => 30,  48 => 25,  46 => 4,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "base_view.twig", "D:\\Archive\\School\\Alfred State\\Sophmore Year\\Spring Semester\\Web Programming II\\Assignments\\GLOs\\Backups\\Glo003\\Glo003\\views\\base_view.twig");
    }
}
