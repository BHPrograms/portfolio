$(document).ready(function() {
    $("#edit_button").click(function() {
        var editID = $("#edit_button").val();
        if ($('#edit_' + editID).valid()) {
            var form = $('#edit_' + editID)[0];
            var formData = new FormData(form);
            formData.append("action", "edit");
            formData.append("ID", editID);
            updateJokes(formData);
        }
    });
});