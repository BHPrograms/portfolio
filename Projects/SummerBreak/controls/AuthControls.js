$(document).ready(function() {

    //Handles all authentication items
    function authenticate(formData) {
        $.ajax({
            url: 'model/php/Authentication.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data) {
                console.log(data);
                if (data == "Account created") {
                    UIkit.notification("Account created successfully.", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (data == "Account could not be created at this time.") {
                    UIkit.notification("Account could not be created at this time.", { status: 'danger' });
                } else if (data == "Username taken.") {
                    $("#signup_error").text(data);
                } else if (data == "Signed in successfully.") {
                    UIkit.notification("Signed in successfully", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else
                if (data == "Incorrect username or password") {
                    $("#signin_error").text(data);
                } else if (data == "Logged out successfully.") {
                    UIkit.notification("Logged out successfully.", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            }
        });
    }

    //Passed information required to authenticate
    $("#btn_signin").click(function() {
        var form = $('#signin_form')[0];
        var formData = new FormData(form);
        formData.append("method", "SignIn");
        authenticate(formData);
    });

    $("#btn_signup").click(function() {
        var form = $('#signup_form')[0];
        var formData = new FormData(form);
        formData.append("method", "SignUp");
        authenticate(formData);
    });

    $("#li_logout").click(function() {
        authenticate();
    });


});