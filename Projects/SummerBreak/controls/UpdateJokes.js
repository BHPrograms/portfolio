function updateJokes(formData) {
    $.ajax({
        url: 'model/php/Jokes.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(response) {
            console.log(response);
            if (response == "Signed out") {
                UIkit.notification("Please sign in", { status: 'danger' });
            } else if (response == "Create success") {
                UIkit.notification("Joke submitted successfully", { status: 'success' });
                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else if (response == "Create failure") {
                UIkit.notification("Joke could not be submitted", { status: 'danger' });
            } else if (response == "Edit success") {
                UIkit.notification("Joke edited successfully", { status: 'success' });
                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else if (response == "Edit failure") {
                UIkit.notification("Joke could not be edited", { status: 'danger' });
            } else if (response == "Delete success") {
                UIkit.notification("Joke deleted successfully", { status: 'success' });
                setTimeout(function() {
                    location.reload();
                }, 2000);
            } else if (response == "Delete failure") {
                UIkit.notification("Joke could not be deleted", { status: 'danger' });
            } else {
                UIkit.notification("An unspecified error has occured", { status: 'danger' });
            }
        }
    });
}