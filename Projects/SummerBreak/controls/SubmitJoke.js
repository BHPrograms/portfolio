$(document).ready(function() {
    $("#submit_button").click(function() {
        if ($('#submit_form').valid()) {
            var form = $('#submit_form')[0];
            var formData = new FormData(form);
            formData.append("action", "create");

            updateJokes(formData);
        }
    });
});