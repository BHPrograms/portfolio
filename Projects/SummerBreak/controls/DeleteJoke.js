$(document).ready(function() {
    $(".delete_button").click(function(event) {
        var formData = new FormData();
        formData.append("action", "delete");
        var editID = event.target.value;
        formData.append("ID", editID);
        updateJokes(formData);

    });
});