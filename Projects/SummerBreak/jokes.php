<?php
    include("model/php/Jokes.php");
    require_once 'vendor/autoload.php';
    
    $loader = new \Twig\Loader\FilesystemLoader('views');
    $twig = new \Twig\Environment($loader, [
        'cache' => false,//'twig_cache',
    ]);

    $loginStatus = false;
    if (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"]) {
        $loginStatus = true;
    }

    $username = false;
    if (isset($_SESSION["user"])) {
        $username = $_SESSION["user"];
    }
    
    echo $twig->render('jokes.twig', ["jokes" => getJokes(), "loginStatus" => $loginStatus, "username" => $username]);
?>