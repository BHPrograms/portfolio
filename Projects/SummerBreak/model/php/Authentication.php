<?php

/* Class used for all basic auth options. Sanitizes and hashes info as appropriate */
class Authentication {

    public function signUp() {
        $user = trim(filter_input(INPUT_POST, 'signup_user', FILTER_SANITIZE_EMAIL));
        $pass = trim(filter_input(INPUT_POST, 'signup_pass', FILTER_SANITIZE_STRING));
        $pass = password_hash($pass, PASSWORD_DEFAULT);

        require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");

        $stmt = $conn->prepare("Select Username
                                FROM SummerBreakUser
                                WHERE Username = :user
                                ;");
        $stmt->bindParam(':user', $user);
        $stmt->execute();

        if ($stmt->rowCount() == 0) { 
            $stmt = $conn->prepare("INSERT INTO SummerBreakUser(Username, PassHash) VALUES(:user, :pass);");
            $stmt->bindParam(':user', $user);
            $stmt->bindParam(':pass', $pass);

            if ($stmt->execute()) {
                echo "Account created";
                $_SESSION["loggedIn"] = true;
                $_SESSION["user"] = $user;
                exit;
            } else {
                echo "Account could not be created at this time.";
                exit;
            }
        } else {
            echo "Username taken.";
            exit;
        }
        
        echo "Account could not be created at this time.";
        exit;
    }

    public function signIn() {
        $user = trim(filter_input(INPUT_POST, 'signin_user', FILTER_SANITIZE_EMAIL));
        $pass = trim(filter_input(INPUT_POST, 'signin_pass', FILTER_SANITIZE_STRING));

        require_once($_SERVER['DOCUMENT_ROOT'] . "/model/php/DatabaseConfig.php");

        $stmt = $conn->prepare("Select Username, PassHash
                                FROM SummerBreakUser
                                WHERE Username = :user
                                ;");
        $stmt->bindParam(':user', $user);
        $stmt->execute();
        if ($stmt->rowCount() == 1) { 
            $result = $stmt->fetch(PDO::FETCH_ASSOC);

            if (password_verify($pass, $result["PassHash"])) {
                echo "Signed in successfully.";
                $_SESSION["loggedIn"] = true;
                $_SESSION["user"] = $user;
                exit;
            } else {
                echo "Incorrect username or password";
                exit;
            }
        } else {
            echo "Incorrect username or password";
            exit;
        }
    }

    public function logOut() {
        $_SESSION["loggedIn"] = false;
        $_SESSION["user"] = "";
        echo "Logged out successfully.";
        exit;
    }
}

if ($_SERVER['REQUEST_METHOD']=='POST') { 
    session_start();
    $userAuth = new Authentication;

    if(isset($_POST['method'])){
        if ($_POST['method'] == "SignUp") {
            $userAuth->signUp($_POST);
        } else {
            $userAuth->signIn($_POST);
        }
    }else {
        $userAuth->logOut($_POST);
    }
}

?>