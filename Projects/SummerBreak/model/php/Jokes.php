<?php
    require_once("DatabaseConfig.php");
    session_start();
    
    function getJokes() {
        global $conn;

        $stmt = $conn->prepare('SELECT * FROM SummerBreakJoke');

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $jokes = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $jokes;
        } else {
            return false;
        }
    }
?>