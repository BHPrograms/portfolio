<?php
    function getActivities() {
        $activities = array();

        $xml_activities = simplexml_load_file("xml/Activities.xml");

        foreach($xml_activities->Activity as $activity) {
            $data = ['thumbnail' => $activity->Thumbnail, 'header' => $activity->Header, 
                'description' => $activity->Description, 'link' => $activity->Link];
            array_push($activities, $data);
        }
        
        return $activities;
    }
?>