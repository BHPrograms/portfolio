/*Despite being labeled as an onAuthStateChanged observer,
this is the only reliable way to retrieve current user.*/
var username;
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        username = user.email;
    } else {
        username = "";
    }
});


function updateArticles(formData) {
    $.ajax({
        url: 'model/php/update_articles.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(JSONArray) {
            console.log(JSONArray);
            var responseArr = JSON.parse(JSONArray);
            var responseStr = responseArr[0];
            if (responseStr == "Create success") {
                UIkit.notification("Article created successfuly", { status: 'success' });
                setTimeout(function() {
                    window.location.replace("article.php?article=" + responseArr[1]);
                }, 2000);
            } else if (responseStr == "Create failure") {
                UIkit.notification("Article could not be created", { status: 'danger' });
            } else if (responseStr == "Edit success") {
                UIkit.notification("Article updated successfuly", { status: 'success' });
                setTimeout(function() {
                    window.location.replace("article.php?article=" + responseArr[1]);
                }, 2000);
            } else if (responseStr == "Edit failure") {
                UIkit.notification("Article could not be updated", { status: 'danger' });
            } else if (responseStr == "Delete success") {
                UIkit.notification("Article deleted successfuly", { status: 'success' });
                setTimeout(function() {
                    window.location.replace("articles.php");
                }, 2000);
            } else if (responseStr == "Delete failure") {
                UIkit.notification("Article could not be deleted", { status: 'danger' });
            }
        }
    });
}