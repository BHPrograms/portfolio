<?php

require_once("DBConfig.php");

function retrieveArticle($articleID) {
    global $conn;
    $stmt = $conn->prepare('SELECT ID, title, author, pub_date, content, 
        AppSoftArticle.thumbnail as articleThumbnail, profileID, name, 
        profile.thumbnail as profileThumbnail FROM AppSoftArticle, profile 
        WHERE ID = :ID AND AppSoftArticle.author = profile.username');
    $stmt->bindParam(":ID", $articleID, PDO::PARAM_INT);

    if ($stmt->execute() && $stmt->rowCount() > 0) {
        $articleData = $stmt->fetch(PDO::FETCH_ASSOC);

        //Separates lead paragraph from the rest of the article
        $articleContents = preg_split('/\r\n|\r|\n/', $articleData['content'], 2);
        $articleData['lead'] = nl2br($articleContents[0]);

        //Technically, an article could be written with no whitespace, only a lead
        if (count($articleContents) > 1) {
            $articleData['content'] = nl2br($articleContents[1]);
        } else {
            $articleData['content'] = "";
        }

        return $articleData;
    } else {
        return false;
    }
}


 $featured = array();
 function displayFeatured($displayCount) {
     try {
         global $conn;
         global $featured;
         
         $sql = 'SELECT profile.profileID, AppSoftArticle.id AS id, profile.name AS author,AppSoftArticle.title,
             AppSoftArticle.thumbnail AS articleThumbnail
                 FROM AppSoftArticle, profile
                 WHERE AppSoftArticle.author = profile.username
                 AND  AppSoftArticle.collection LIKE "%Featured%"
                 ORDER BY AppSoftArticle.id DESC
                 ;';
         
         $articleIndex = 0;
         foreach($conn->query($sql) as $row) {
             $featured[$articleIndex] = array();
             $featured[$articleIndex]['id'] = $row['id'];
             $featured[$articleIndex]['title'] = $row['title'];
             $featured[$articleIndex]['author'] = $row['author'];
             $featured[$articleIndex]['profileID'] = $row['profileID'];
             $featured[$articleIndex]['articleThumbnail'] = $row['articleThumbnail'];
         
             $articleIndex++;
         }
     } catch (PDOException $e) {
         $errorName = "PDOException";
                     $errorTag = "applications.php, ReleasedApplications:";
                     $errorMessage = $e->getMessage();
                     
         include_once("php/Logger.php");
         logErr($errorName, $errorTag, $errorMessage);
     }

     if (count($featured) > 0) {
         echo '<h3>Featured Articles</h3>';
         
         if (!isset($displayCount)) {
             $displayCount = count($featured);
         }

         for($i = 0; $i < $displayCount; $i++) {
             include("views/articles//featured_item.php");
         }
     }
 }

 function displayCollections() {
     try {
        global $conn;

        //Retrives description of AppSoftArticle.collection
        $stmt = $conn->prepare('DESCRIBE AppSoftArticle collection;');
        $stmt->execute();
        $rowData = $stmt->fetch(PDO::FETCH_ASSOC);
        $collectionData = $rowData['Type'];

        /*There is no elegant way to parse the metadata
        retrieved. Metadata is retrieved in the format of
        set('Val1','Val2','Val3). preg_matc removes set(' and '),
        which leaves all data delimited by ','. Regex
        split uses this to retrieve all collection
        options.*/
        $rawCollections = array();
        preg_match('/set\(\'(.*)\'\)/', $collectionData, $rawCollections);
        $collections = preg_split("/','/", $rawCollections[1]);

        foreach($collections as $collection) {
            //PDO Prepared Statements hate binding values between wildcards.
            $collectionPattern = '%' . $collection .'%';
            $stmt = $conn->prepare('SELECT profile.profileID, AppSoftArticle.id, AppSoftArticle.title, profile.name, AppSoftArticle.thumbnail 
                FROM AppSoftArticle, profile WHERE AppSoftArticle.author = profile.username AND AppSoftArticle.collection LIKE :collection ORDER BY ID DESC');
            $stmt->bindParam(":collection", $collectionPattern);
            $stmt->execute();
            $collectionArticles = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                //Start of collection slideshow container
                echo '<h1 style="color:black;" class="uk-text-center">' . $collection . '</h1>
                      <div uk-slider="center: true" tabindex="-1">
                        <div class="uk-position-relative uk-visible-toggle uk-light">
                        <ul class="uk-slider-items uk-grid-match uk-child-width-1-2@s uk-grid">';

                foreach($collectionArticles as $article) {
                    $thumbnail = $article['thumbnail'];
                    $title = $article['title'];
                    $author = $article['name'];
                    $profileID = $article['profileID'];
                    $id = $article['id'];
                    include('views/articles/collection_slide.php');
                }

                //End of collection slideshow container
                echo '</ul>
                        <a class="uk-position-center-left uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                        <a class="uk-position-center-right uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-next uk-slider-item="next"></a>
                    </div>
                    </div>';
            }      
        }

     } catch (PDOException $e) {
         $errorName = "PDOException";
                     $errorTag = "applications.php, ReleasedApplications:";
                     $errorMessage = $e->getMessage();
                     
         include_once("php/Logger.php");
         logErr($errorName, $errorTag, $errorMessage);
     }
 }

 function displayIndustryArticles() {
    try {
       global $conn;

      //PDO Prepared Statements hate binding values between wildcards.
      $collectionPattern = '%Industry Articles%';
      $stmt = $conn->prepare('SELECT profile.profileID, AppSoftArticle.id, AppSoftArticle.title, profile.name, AppSoftArticle.thumbnail 
          FROM AppSoftArticle, profile WHERE AppSoftArticle.author = profile.username AND AppSoftArticle.collection LIKE :collection ORDER BY ID DESC');
      $stmt->bindParam(":collection", $collectionPattern);
      $stmt->execute();
      $collectionArticles = $stmt->fetchAll(PDO::FETCH_ASSOC);

      if ($stmt->rowCount() > 0) {
          //Start of collection slideshow container
          echo '<h1 style="color:black;" class="uk-text-center">Industry Articles</h1>
                <div uk-slider="center: true" tabindex="-1">
                  <div class="uk-position-relative uk-visible-toggle uk-light">
                  <ul class="uk-slider-items uk-grid-match uk-child-width-1-2@s uk-grid">';

          foreach($collectionArticles as $article) {
              $thumbnail = $article['thumbnail'];
              $title = $article['title'];
              $author = $article['name'];
              $profileID = $article['profileID'];
              $id = $article['id'];
              include('views/articles/collection_slide.php');
          }

          //End of collection slideshow container
          echo '</ul>
                  <a class="uk-position-center-left uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-next uk-slider-item="next"></a>
              </div>
              </div>';
      }

    } catch (PDOException $e) {
        $errorName = "PDOException";
                    $errorTag = "applications.php, ReleasedApplications:";
                    $errorMessage = $e->getMessage();
                    
        include_once("php/Logger.php");
        logErr($errorName, $errorTag, $errorMessage);
    }
}
 function displayUserArticles($profileID) {
    try {
       global $conn;
        $stmt = $conn->prepare('SELECT profile.profileID, AppSoftArticle.id, AppSoftArticle.title, profile.name, AppSoftArticle.thumbnail 
            FROM AppSoftArticle, profile WHERE AppSoftArticle.author = profile.username AND profile.profileID = :profileID');
        $stmt->bindParam(":profileID", $profileID);
        $stmt->execute();
        $userArticles = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($stmt->rowCount() > 0) {
            //Start of collection slideshow container
            echo '<h1 style="color:black;" class="uk-text-center">User Articles</h1>
                    <div uk-slider="center: true" tabindex="-1">
                    <div class="uk-position-relative uk-visible-toggle uk-light">
                    <ul class="uk-slider-items uk-grid-match uk-child-width-1-2@s uk-grid">';

            foreach($userArticles as $article) {
                $thumbnail = $article['thumbnail'];
                $title = $article['title'];
                $author = $article['name'];
                $profileID = $article['profileID'];
                $id = $article['id'];
                include('views/articles/user_article_slide.php');
            }

            //End of collection slideshow container
            echo '</ul>
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover transparent-black" href="#" uk-slidenav-next uk-slider-item="next"></a>
                </div>
                </div>';
        }      
    } catch (PDOException $e) {
        $errorName = "PDOException";
                    $errorTag = "applications.php, ReleasedApplications:";
                    $errorMessage = $e->getMessage();
                    
        include_once("php/Logger.php");
        logErr($errorName, $errorTag, $errorMessage);
    }
}
?>