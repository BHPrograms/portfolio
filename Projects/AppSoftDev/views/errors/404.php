
<div class="uk-container uk-text-center">
    <h1 class="uk-heading-xlarge"><?php echo $contentType; ?> not found.</h1>
    <h1 class="uk-heading-small">Why not check out <a href="<?php echo $redirect; ?>"> <?php echo $redirect_text; ?></a> instead?</h1>
</div>