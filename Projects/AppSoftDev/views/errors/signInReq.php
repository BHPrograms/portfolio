
<div class="uk-container uk-text-center">
    <h1 class="uk-heading-xlarge">Oops!</h1>
    <h1 class="uk-heading-small">You must be signed in to access this content.</h1>
</div>