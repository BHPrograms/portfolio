<div class="uk-container uk-container-medium">

    <!--Page/Article header-->
    <div id="article_thumbnail"
        class="uk-height-medium uk-flex uk-flex-bottom uk-flex-center uk-flex-middle uk-background-cover uk-light"
        data-src="<?php echo addslashes($article['articleThumbnail']); ?>" uk-img>
        <div style="width: 100%;" class="uk-text-center">
            <div class="transparent-black" style="width: 100%;">
                <h1 id="article_title" style="margin: 0; padding: 0;"><?php echo $article['title']; ?></h1>
                <p id="article_info" class="white-text" style="margin: 0; padding: 0;">
                    Published on <span id="article_date"><?php echo $article['pub_date']; ?></span> by <?php echo $article['author']; ?>
                </p>
            </div>
        </div>
    </div>

    <div class="uk-flex" style="margin-top: 2%;">
        <!--Article contents-->
        <article class="uk-article uk-width-3-4">
            <p id="article_lead" class="uk-text-lead" style="line-height: 2;"><?php echo $article['lead']; ?></p>
            <p id="article_content" class="uk-text-break" style="font-size: large; line-height: 2; color: black;">
                <?php echo $article['content']; ?></p>
        </article>

        <!--Article recommendations-->
        <div class="uk-width-1-4 uk-text-center">
            <?php 
                    include_once("model/php/articles.php"); 
                    displayFeatured(3);
            ?>
        </div>
    </div>
</div>
