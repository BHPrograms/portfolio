 <li>
     <a href="article.php?article=<?php echo $id; ?>">
         <div class="uk-card uk-card-default" style="margin: 1%;">
             <div class="uk-card-media-top">
                 <img src="<?php echo $thumbnail; ?>" style="width: 100%;" class="uk-height-medium" uk-img>
             </div>
             <div class="uk-card-body uk-text-center">
                 <h1 class="uk-margin-remove black-text"><?php echo $title; ?></h1>
                 <p class="uk-margin-remove black-text">
                    <a class="black-text" href="profile.php?profileID=<?php echo $profileID; ?>">By <?php echo $author; ?></a>
                </p>
             </div>
         </div>
     </a>
 </li>