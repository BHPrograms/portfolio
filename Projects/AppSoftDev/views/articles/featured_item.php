<div style="padding: 2%;">
    <div class="uk-align-center uk-flex uk-flex-bottom uk-flex-center" style='width: 250px; height: 250px; background: url("<?php echo addslashes($featured[$i]['articleThumbnail']);?>") 50% 50% no-repeat;'>
        <div class="transparent-black" style="width: 100%;">
            <a href="article.php?article=<?php echo $featured[$i]['id']; ?>">
                <h4 class="white-text"><?php echo $featured[$i]['title']; ?></h4>
            </a>
            <p class="white-text">By 
                <a class="white-text" href="profile.php?profileID=<?php echo $featured[$i]['profileID']; ?>"><?php echo $featured[$i]['author']; ?></a>
            </p>
        </div>
    </div>
    
</div>