<div class="uk-container">
    <div class="uk-text-center" style="margin: 2%;">
        <h1 class="uk-heading-medium">Our Articles</h1>
        <div class="uk-align-center" style="width: 50%;">
            <p class="uk-text-large">For those interested in Application Software Development, check out our articles talking about different careers in the industry.</p>
        </div>
    </div>
    <?php
        include_once("model/php/articles.php");
        displayCollections();
    ?>
</div>