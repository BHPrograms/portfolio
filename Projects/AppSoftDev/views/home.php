<head>
    <title>App Soft Dev</title>
</head>

<main class="uk-container">
    <!--Slideshow advertising site content-->
    <div id="slideshow" class="uk-position-relative uk-visible-toggle" tabindex="0"
        uk-slideshow="autoplay-interval: 5000" style="margin-top: 2%;">
        <ul class="uk-slideshow-items">
            <li>
                <a href="articles.php">
                    <img src="img/app_dev_slides/industry.jfif" alt="" uk-cover>
                    <div class="uk-overlay uk-overlay-primary uk-position-bottom uk-text-center">
                        <h3 class="uk-margin-remove">Learn About Career Options</h3>
                        <p class="uk-margin-remove">A degree in App Soft Dev opens up many career options.</p>
                    </div>
                </a>
            </li>
        </ul>
        <div class="uk-light">
            <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous
                uk-slideshow-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next
                uk-slideshow-item="next"></a>
        </div>
    </div>

    <div class="uk-box-shadow-large">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h1 class="uk-heading-line uk-text-center white-text"><span>About Software Development</span></h1>
        </div>

        <div style="padding: 1%;">
            <p class ="uk-text-break ">Software development is a versatile industry, with many different career options. 
            Those in the field can specialize in a specific type of development, try their hand at many different kinds, or even 
            work in related fields such as database administration. For those interested, it may be worth considering a BTech in 
            Application Software Development. Articles talking about careers in the field may be found <a href="articles.php">here</a></p>
        </div>

    </div>


</main>