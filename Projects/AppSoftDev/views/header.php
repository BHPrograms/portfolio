<!DOCTYPE html>
<html>
<head>
    <title>App Soft Dev</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js"></script>

    <!--JQuery CDN-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link href="css/palette.css" rel="stylesheet">
    <link href="css/helpers.css" rel="stylesheet">

    <meta charset="utf-8" />
</head>

<body>
    <?php 
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    ?>
    <div class="page-wrapper">
        <nav class="uk-navbar-container purple uk-box-shadow-large" uk-navbar>
            <a class="uk-navbar-item uk-logo white-text" href="index.php">App Soft Development</a>
            <div class="uk-navbar-right" style="margin-right: 25px;">
                <ul class="uk-navbar-nav">
                    <li><a href="index.php" class="white-text uk-box-shadow-hover-large">Home</a></li>
                    <li><a href="articles.php" class="white-text uk-box-shadow-hover-large">Articles</a></li>
                </ul>

            </div>
        </nav>