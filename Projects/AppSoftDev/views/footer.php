
            <footer class="purple" style="margin-top: 2%;">
                <div class="uk-flex uk-flex-around">
                    <div class="">
                        <h3 class="white-text">BHPrograms</h3>
                        <ul class="uk-list">
                            <li><a class="white-text" href="https://www.bhprogramsdevelopment.com">BHProgramsDevelopment.com</a>
                            </li>
                            <li class="white-text"><a class="white-text" href="tel:6073013986">Phone: 607-301-3986</a></li>
                            <li class="white-text"><a class="white-text" href="mailto:info@bhprogramsdevelopment.com">Email:
                                    Info@BHProgramsDevelopment.com</a></li>
                        </ul>
                    </div>
                    <div class="">
                        <h3 class="white-text">Articles</h3>
                        <ul class="uk-list">
                            <li>
                                <a class="white-text" href="articles.php">All Articles</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>