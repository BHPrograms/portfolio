<head>
    <script src="controllers/user_courses.js"></script>
</head>

<div class="uk-container">
    <div class="uk-flex">
        <h2 class="uk-text-center">
            <p>My Courses</p>
        </h2>
        <div style="padding: 10px;">
            <a uk-toggle="target: #add-course" uk-icon="icon: pencil"></a>
            <div id="add-course" uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <h2 class="uk-modal-title">Add a Course</h2>
                    <form id="add-courses">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Course Name</label>
                            <div class="uk-form-controls">
                                <input name="course" class="uk-input" type="text" placeholder="Course">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Course Number</label>
                            <div class="uk-form-controls">
                                <input name="crn" class="uk-input" type="text" placeholder="CRN">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Semester</label>
                            <div class="uk-form-controls">
                                <input name="semester" class="uk-input" type="text" placeholder="Semester Taken">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Professor</label>
                            <div class="uk-form-controls">
                                <input name="prof" class="uk-input" type="text" placeholder="Professor's Name">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <label class="uk-form-label" for="form-stacked-text">Course Description</label>
                            <textarea name="desc" class="uk-textarea" rows="5"></textarea>
                        </div>
                        <div class="uk-margin">
                            <button id="add_course" class="uk-button uk-button-default" type="button">Add
                                Course</button>
                        </div>
                    </form>
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                </div>
            </div>
        </div>
    </div>

    <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slider>
        <ul class="uk-slider-items uk-container" style="padding: 2%;">
            <?php
                                //Gets college info
                                $courses = simplexml_load_file("xml/UserCourses.xml");    
                                foreach($courses->Courses->Class as $class) {
                                    $crn = $class->CRN;
                                    $courseName = $class->Name;
                                    $term = $class->Semester;
                                    $prof = $class->Professor;
                                    $courseDescription = $class->Description;
                                    
                                    include("views/usercourse.php");
                                }
                                ?>
        </ul>

        <a style="color: white;" class="uk-position-center-left uk-position-small uk-hidden-hover" href="#"
            uk-slidenav-previous uk-slider-item="previous"></a>
        <a style="color: white;" class="uk-position-center-right uk-position-small uk-hidden-hover" href="#"
            uk-slidenav-next uk-slider-item="next"></a>
    </div>
</div>