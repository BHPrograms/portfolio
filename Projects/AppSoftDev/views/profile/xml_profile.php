<?php include("model/php/basic_student_info.php"); ?>

<div class="uk-flex">
    <div style="width: 70%; padding: 2%;"><span>I'm <?php echo $name; ?>, the developer of Dev for Dummies. I go to <?php echo $uni; ?> to
            study <?php echo $major; ?>
            at the <?php echo $level; ?> level in their <?php echo $dept; ?> department. I'm currently a <?php echo 
        $standing; ?> and hope to graduate <?php echo $grad; ?>. I created this website as part of a class project.
         I'm happy to answer any questions aspiring devs may have, and can be reached at <?php echo $email; ?> You can find out
         more about my personal experience below, or even browse our <a href="articles.php">articles</a> and see what I've 
         written.</span></div>
    <div><img class="uk-align-right" src="<?php echo $photo; ?>"></div>
</div>