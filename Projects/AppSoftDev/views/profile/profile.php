<head>
    <script src="controllers/profile.js"></script>
</head>

<div class="uk-container" style="margin-top: 5%; margin-bottom: 5%;">
    <div class="uk-grid-small" uk-grid>
        <div class="uk-width-3-4">
            <div id="profile_header" style="inline-block">
                <h1 class="uk-heading-medium uk-text-middle" style="display: inline;"><?php echo $profile['name']; ?></h1>
            </div>
            <p class="uk-text-lead"><?php echo $profile['bio']; ?></p>
        </div>
        <div class="uk-width-1-4">
        <img class="uk-border-circle uk-align-center" style="width: 100%; height: 100%;" src="<?php echo addslashes($profile["thumbnail"]); ?>">
        </div>
    </div>
    <hr class="uk-divider-icon">
</div>