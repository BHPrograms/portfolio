<?php
    //Evals not signed in, signed in but no profile, signed in with profile
    if (!isset($_SESSION["userProfile"])) {
        $thumbnail = "img/defaults/profile_default.png";
    } else if ($_SESSION["userProfile"]["exists"] == false) {
        $thumbnail = "img/defaults/profile_default.png";
    } else {
        $thumbnail = addslashes($_SESSION["userProfile"]["thumbnail"]);
    }
?>


<a class="white-text uk-box-shadow-hover-large">
    <img class="uk-border-circle" style="width: 64px; height: 64px;" src="<?php echo $thumbnail; ?>">
</a>
<div uk-dropdown="pos: bottom-justify; boundary: .uk-navbar-container;" class="uk-width-1-6 ">
    <ul class="uk-nav uk-dropdown-nav">
        <li>
            <a class="black-text" href="profile.php?profileID=<?php 
                /*This is never shown to the user while logged out,
                but hidden or not you don't want errors*/
                 if (isset($_SESSION["userProfile"])) {
                    echo trim($_SESSION["userProfile"]["profileID"]);
                } 
            ?>">My Profile</a></li>
            <li>
            <a class="black-text" href="create_article.php">Write an article</a></li>
        <li id="li_logout"><a class="uk-text-danger uk-text-center" href="#">Log Out</a></li>
    </ul>
</div>