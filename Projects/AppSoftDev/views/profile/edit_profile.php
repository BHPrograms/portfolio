<head>
    <script src="model/js/profiles.js"></script>
    <script src="controllers/profile_edit.js"></script>
</head>

<div class="uk-container">
    <div class="uk-text-center" style="margin: 2%;">
        <h1 class="uk-heading-medium">Edit Profile</h1>
    </div>

    <div class="uk-container">
        <form id="edit_profile_form">
            <div uk-grid class="uk-resize-vertical">
                <div class="uk-width-3-4">
                    <label class="uk-form-label uk-text-lead" for="author_name">Full Name</label>
                    <br>
                    <input id="edit_author_name" name="edit_author_name" class="uk-input" type="text"
                        style="width: 500px;" value="<?php echo $profile['name']; ?>">
                    <br>
                    <label class="uk-form-label uk-text-lead" for="edit_author_bio">Bio</label>
                    <br>
                    <textarea id="edit_author_bio" name="edit_author_bio" class="uk-textarea"
                        style="width: 500px; height: 200px; resize: none;"><?php echo $profile['bio']; ?></textarea>
                </div>
                <div class="uk-width-1-4">
                    <p class="uk-text-lead uk-text-center">Profile Picture</p>
                    <img id="edit_profile_photo" class="uk-border-circle uk-align-center"
                        style="width: 50%; height: 50%;" src="<?php echo addslashes($profile["thumbnail"]); ?>">
                    <div uk-form-custom="target: true" class="uk-align-center uk-text-center">
                        <input id="edit_profile_thumbnail" name="edit_profile_thumbnail" type="file">
                        <input class="uk-input uk-form-width-medium" type="text" placeholder="Select profile picture"
                            disabled>
                    </div>
                </div>
            </div>
            <div class="uk-flex uk-flex-center" style="margin: 10px;">
                <button class="uk-button uk-text-capitalize uk-button-danger" type="button" href="#delete-modal"
                    uk-toggle>Delete Profile</button>
                <div id="delete-modal" class="uk-flex-top" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
                        <button class="uk-modal-close-default" type="button" uk-close></button>
                        <h4 class="uk-text-danger uk-text-center">WARNING: You're about to delete your profile</h4>
                        <p>Are you sure you would like to delete your profile? Doing so will also delete any articles
                            you have published. If you are sure you'd like to continue, click the button below.</p>
                        <div class="uk-flex uk-flex-center">
                            <button id="btn_delete_profile"
                                class="uk-button uk-text-capitalize uk-button-danger uk-align-center" type="button"
                                style="margin: 10px;">Permanently Delete</button>
                        </div>
                        <div class="uk-flex uk-flex-center">
                            <button class="uk-button uk-modal-close uk-align-center"
                                type="button" style="margin: 10px;">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
            <div class="uk-flex uk-flex-center" style="margin: 10px;">
                <button id="btn_edit_profile" class="uk-button uk-text-capitalize" type="button">Submit</button>
            </div>
        </form>
    </div>

    <!--Validate in .validate-->
    <script type="text/javascript">
    $.validator.setDefaults({
        success: "valid"
    });

    //Custom name validation
    $.validator.addMethod("edit_author_name", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{3,25}$/).test(value);
    }, "Your full name must be between 3 and 25 characters, and cannot contain slashes or angle brackets.");

    //Custom bio validation
    $.validator.addMethod("edit_author_bio", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{10,500}$/).test(value);
    }, "Your bio must be between 10 and 500 characters, and cannot contain slashes or angle brackets.");


    //Set form rules, check validity
    $("#edit_profile_form").validate({
        rules: {
            edit_author_name: "required edit_author_name",
            edit_author_bio: "required edit_author_bio",
        }
    });
    </script>
</div>