<head>
    <script src="model/js/profiles.js"></script>
    <script src="controllers/profile_creation.js"></script>
</head>

<div class="uk-container">
    <div class="uk-text-center" style="margin: 2%;">
        <h1 class="uk-heading-medium">Create Your Profile</h1>
        <div class="uk-align-center" style="width: 50%;">
            <p class="uk-text-large">Before you can start writing, you need to create a profile. Why not let readers
                know who you are?</p>
        </div>
    </div>

    <div class="uk-container">
        <form id="create_profile_form">
            <div uk-grid class="uk-resize-vertical">
                <div class="uk-width-3-4">
                    <label class="uk-form-label uk-text-lead" for="author_name">Full Name</label>
                    <br>
                    <input id="author_name" name="author_name" class="uk-input" type="text"
                        style="width: 500px;">
                    <br>
                    <label class="uk-form-label uk-text-lead" for="edit_author_bio">Bio</label>
                    <br>
                    <textarea id="author_bio" name="author_bio" class="uk-textarea"
                        style="width: 500px; height: 200px; resize: none;"></textarea>
                </div>
                <div class="uk-width-1-4">
                    <p class="uk-text-lead uk-text-center">Profile Picture</p>
                    <img id="profile_photo" class="uk-border-circle uk-align-center"
                        style="width: 50%; height: 50%;" src="img/defaults/profile_default.png">
                    <div uk-form-custom="target: true" class="uk-align-center uk-text-center">
                        <input id="author_thumbnail" name="author_thumbnail" type="file">
                        <input class="uk-input uk-form-width-medium" type="text" placeholder="Select profile picture"
                            disabled>
                    </div>
                </div>
            </div>
            <div class="uk-flex uk-flex-center" style="margin: 10px;">
                <button id="btn_submit" class="uk-button uk-text-capitalize" type="button">Submit</button>
            </div>
        </form>
    </div>

    <!--Validate in .validate-->
    <script type="text/javascript">
    $.validator.setDefaults({
        success: "valid"
    });

    //Custom name validation
    $.validator.addMethod("author_name", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{3,25}$/).test(value);
    }, "Your full name must be between 3 and 25 characters, and cannot contain slashes or angle brackets.");

    //Custom bio validation
    $.validator.addMethod("author_bio", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{10,500}$/).test(value);
    }, "Your bio must be between 10 and 500 characters, and cannot contain slashes or angle brackets.");


    //Set form rules, check validity
    $("#create_profile_form").validate({
        rules: {
            author_name: "required author_name",
            author_bio: "required author_bio"
        }
    });
    </script>
</div>