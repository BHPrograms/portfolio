<?php
    include("views/header.php");


    /*Used to evaluate if article exists,
    and retrieve content if so*/
    include_once("model/php/articles.php");

    $articleID = "";
    if (isset($_GET['article'])) {
        $articleID = $_GET['article'];
    }

    /*Any nonnumeric article ID will convert to
    0 and retrieve article 0. Instead, it will
    stay false, and display 404. False is also 
    returned when article doesn't exist*/
    $article = false;
    if (is_numeric($articleID)) {
        $article = retrieveArticle($articleID);
    }

    if ($article != false) {
        include("views/articles/article.php");
    } else {
        $contentType = "Article";
        $redirect = "articles.php";
        $redirect_text = "our articles";
        include("views/errors/404.php");
    }

    include("views/footer.php");
?>