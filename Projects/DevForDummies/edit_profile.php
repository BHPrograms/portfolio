<?php
    include("views/header.php");

    if (!isset($_SESSION["userProfile"])) {
        include("views/errors/signInReq.php");
    } else {
         /*Used to evaluate if article exists,
        and retrieve content if so*/
        include_once("model/php/profiles.php");

        //False by default and if profile not found
        $profile = false;
        if (isset($_GET['profileID'])) {
            $profileID = $_GET['profileID'];
            $profile = retrieveProfile($profileID);
        }
        

        if ($profile != false) {
            include("views/profile/edit_profile.php");
        } else {
            /*No current plans for searching profiles,
            or displaying all profiles, this redirect
            is intentional.*/
            $contentType = "Profile";
            $redirect = "articles.php";
            $redirect_text = "our articles";
            include("views/errors/404.php");
        }
    }

    include("views/footer.php");
?>