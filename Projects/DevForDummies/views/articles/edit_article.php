<head>
    <script src="model/js/articles.js"></script>
    <script src="controllers/article_edit.js"></script>
</head>

<div class="uk-container">
    <div class="uk-text-center" style="margin: 2%;">
        <h1 class="uk-heading-medium">Edit Article</h1>
    </div>

    <form id="edit_article_form" class="uk-align-center uk-width-xlarge">
        <div style="margin: 10px;">
            <label class="uk-form-label uk-text-lead" for="edit_article_title">Article Title</label>
            <br>
            <input id="edit_article_title" name="edit_article_title" class="uk-input" type="text" style="width: 500px;"
                value="<?php echo $article["title"]; ?>">
        </div>

        <div style="margin: 10px;">
            <label class="uk-form-label uk-text-lead" for="edit_article_content">Article Content</label>
            <br>
            <textarea id="edit_article_content" name="edit_article_content" class="uk-textarea"
                style="width: 500px; height: 200px; resize: none;"><?php echo $article["lead"] . $article["content"];?></textarea>
        </div>

        <div uk-form-custom="target: true" style="margin: 10px">
            <input type="file" name="edit_article_thumbnail">
            <input class="uk-input uk-form-width-medium" type="text" placeholder="Select new thumbnail" disabled>
        </div>

        <div class="uk-flex uk-flex-center" style="margin: 10px;">
            <button class="uk-button uk-text-capitalize uk-button-danger" type="button" href="#delete-modal"
                uk-toggle>Delete Article</button>
            <div id="delete-modal" class="uk-flex-top" uk-modal>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <h4 class="uk-text-danger uk-text-center">WARNING: You're about to delete this article</h4>
                    <p>Are you sure you would like to delete this article? This cannot be reversed. 
                        Click the button below to continue.</p>
                    <div class="uk-flex uk-flex-center">
                        <button id="btn_delete_article"
                            class="uk-button uk-text-capitalize uk-button-danger uk-align-center" type="button"
                            style="margin: 10px;">Permanently Delete</button>
                    </div>
                    <div class="uk-flex uk-flex-center">
                        <button class="uk-button uk-modal-close uk-align-center" type="button"
                            style="margin: 10px;">Cancel</button>
                    </div>

                </div>
            </div>
        </div>


        <div class="uk-flex uk-flex-center" style="margin: 10px;">
            <button id="btn_update" class="uk-button uk-text-capitalize" type="button"
                style="margin: 2%;">Update</button>
        </div>
    </form>

    <!--Validate in .validate-->
    <script type="text/javascript">
    $.validator.setDefaults({
        success: "valid"
    });

    //Custom name validation
    $.validator.addMethod("edit_article_title", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{3,25}$/).test(value);
    }, "Your full name must be between 3 and 25 characters, and cannot contain slashes or angle brackets.");

    //Custom bio validation
    $.validator.addMethod("edit_article_content", function(value, element) {
        return this.optional(element) || new RegExp(/^[^/////</>]{500,10000}$/).test(value);
    }, "Your bio must be between 10 and 500 characters, and cannot contain slashes or angle brackets.");


    //Set form rules, check validity
    $("#edit_article_form").validate({
        rules: {
            edit_article_title: "required edit_article_title",
            edit_article_content: "required edit_article_content",
        }
    });
    </script>
</div>