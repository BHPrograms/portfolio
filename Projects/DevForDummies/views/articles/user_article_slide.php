<li>
    <div class="uk-card uk-card-default uk-text-truncate" style="margin: 1%;">
        <div class="uk-card-media-top article_slide_media">
            <a href="edit_article.php?article=<?php echo $id; ?>">
                <span class="transparent-black uk-position-top-right user-article-edit"
                    uk-icon="icon: pencil; ratio: 2;" style="display: none;"></span>
            </a>
            <a href="article.php?article=<?php echo $id; ?>">
                <img src="<?php echo $thumbnail; ?>" style="width: 100%;" class="uk-height-medium" uk-img>
            </a>
        </div>
        <div class="uk-card-body uk-text-center">
            <a href="article.php?article=<?php echo $id; ?>">
                <h1 class="uk-margin-remove black-text"><?php echo $title; ?></h1>
            </a>
            <p class="uk-margin-remove black-text">
                <a class="black-text" href="profile.php?profileID=<?php echo $profileID; ?>">By
                    <?php echo $author; ?></a>
            </p>
        </div>
    </div>
</li>