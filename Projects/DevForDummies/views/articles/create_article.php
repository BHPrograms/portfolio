<head>
    <script src="model/js/articles.js"></script>
    <script src="controllers/article_submission.js"></script>
</head>

<div class="uk-container">
    <div class="uk-text-center" style="margin: 2%;">
        <h1 class="uk-heading-medium">Our Articles</h1>
        <div class="uk-align-center" style="width: 50%;">
            <p class="uk-text-large"> We have a variety of articles, and accept user submissions as well. Why not get
                started on one or write your own?</p>
        </div>
    </div>

    <form id="create_article_form" class="uk-align-center uk-width-1-2">
        <div style="margin: 10px;">
            <label class="uk-form-label uk-text-lead" for="new_article_title">Article Title</label>
            <br>
            <input id="new_article_title" name="article_title" class="uk-input" type="text" style="width: 500px;">
        </div>

        <div style="margin: 10px;">
            <label class="uk-form-label uk-text-lead" for="new_article_content">Article Content</label>
            <br>
            <textarea id="new_article_content" name="article_content" class="uk-textarea"
                style="width: 500px; height: 200px; resize: none;"></textarea>
        </div>

        <div uk-form-custom="target: true" style="margin: 10px">
            <input type="file" id="article_thumbnail" name="article_thumbnail">
            <input class="uk-input uk-form-width-medium" type="text" placeholder="Select thumbnail" disabled>
        </div>

        <div class="uk-flex uk-flex-center" style="margin: 10px;">
            <button id="btn_upload" class="uk-button uk-text-capitalize" type="button"
                style="margin: 2%;">Upload</button>
        </div>
    </form>

    <!--Validate in .validate-->
    <script type="text/javascript">
    $.validator.setDefaults({
        success: "valid"
    });

    //Custom title validation
    $.validator.addMethod("article_title", function(value, element) {
        return this.optional(element) || new RegExp("([^/////<//>/////]{10,250})").test(value);
    }, "The title must be between 10 and 250 characters, and cannot contain slashes or angle brackets.");

    //Custom article validation
    $.validator.addMethod("article_content", function(value, element) {
        return this.optional(element) || new RegExp("([^/////<//>/////]{500,10000})").test(value);
    }, "The article must be between 500 and 10,000 characters, and cannot contain slashes or angle brackets.");

    //Set form rules, check validity
    $("#create_article_form").validate({
        rules: {
            article_title: "required article_title",
            article_content: "required article_content"
        }
    });
    </script>
</div>