<li class="uk-width-1-1 uk-text-center">
    <div uk-grid>
        <div>
            <p><?php echo $crn; ?></p>
        </div>
        <p>|</p>
        <div>
            <p><?php echo $courseName; ?></p>
        </div>
    </div>

    <p class="uk-text-left">Offered <?php echo $term; ?> by <?php echo $prof; ?></p>
    <div style="padding-left: 15%; padding-right: 15%;">
        <p><?php echo $courseDescription; ?></p>
    </div>
</li>