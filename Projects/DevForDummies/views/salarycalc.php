<head>
    <script src="controllers/salary_calculator.js"></script>
</head>

<div class="uk-container">
    <div class="uk-text-center">
        <h1>Salary Calculation</h1>
        <div class="uk-flex uk-flex-center">
            <div class="uk-width-1-2">
                <p>Many starting development positions pay wages by the hours. If you have a position in mind, you can
                    calculate your salary here based on the hours and wage.</p>
            </div>
        </div>
    </div>

    <div class="uk-flex uk-flex-center">
        <form class="uk-text-center">
            <div class="uk-margin">
                <input id="salary_wage" class="uk-input uk-form-width-medium" type="text" placeholder="Hourly Wage">
            </div>
            <div class="uk-margin">
                <input id="salary_hours" class="uk-input uk-form-width-medium" type="text" placeholder="Hours Per Week">
            </div>
            <div class="uk-margin">
                <p id="salary_info" class="uk-text-lead"></p>
            </div>
            <div class="uk-margin uk-flex uk-flex-center">
                <button id="calculate_salary" class="uk-button uk-button-default" type="button">Calculate</button>
            </div>
        </form>
    </div>

</div>