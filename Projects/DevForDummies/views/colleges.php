<?php
    //Gets college info
    $colleges = simplexml_load_file("xml/CoursesByCollege.xml");

    //Prints a section for each college
    foreach($colleges->College as $college) {
        $collegeName = $college->Name;
        $collegePic = $college->Photo;
        
        include("college_courses.php");
    }
?>