<li class="uk-width-1-1 uk-text-center">
    <div>
        <h3 class="uk-text-center">
            <p><?php echo $careerName; ?></p>
        </h3>
        <div class="uk-align-center" style="width: 75%;">
            <p><?php echo $careerDesc; ?></p>
        </div>
    </div>
    <div class="uk-flex uk-flex-center">
        <div class="uk-width-2-3">
            <h3>Career Technologies</h3>
            <ul uk-accordion>
                <?php
                    foreach($career->Technologies->Technology as $technology) {
                        $technologyName = $technology->Name;
                        $technologyDesc = $technology->Description;
                        include("technology.php");
                    }
                ?>
            </ul>
        </div>
    </div>
</li>