<li class="uk-width-1-1 uk-text-center">
    <button class="course_delete uk-position-top-right" type="button" uk-icon="icon: trash" value="<?php echo $courseName; ?>"></button>
    <br>
    <div uk-grid>
        <div>
            <p><?php echo $crn; ?></p>
        </div>
        <p>|</p>
        <div>
            <p><?php echo $courseName; ?></p>
        </div>
    </div>

    <p class="uk-text-left">Offered <?php echo $term; ?> by <?php echo $prof; ?></p>
    <div style="padding-left: 15%; padding-right: 15%;">
        <p><?php echo $courseDescription; ?></p>
    </div>
</li>