<head>
    <title>Personal Experience</title>
    <script src="js/UIKit/slider.js"></script>
</head>



<main class="uk-container">
    <div class="uk-box-shadow-large">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h1 class="uk-heading-line uk-text-center white-text">
                <p>My Experience</p>
            </h1>
        </div>

        <?php include("model/php/basic_student_info.php"); ?>

        <div class="uk-flex">
            <div style="width: 70%; padding: 2%;"><span>I'm <?php echo $name; ?>, the developer of Dev for Dummies. I go to <?php echo $uni; ?> to
                    study <?php echo $major; ?>
                    at the <?php echo $level; ?> level in their <?php echo $dept; ?> department. I'm currently a <?php echo 
                $standing; ?> and hope to graduate <?php echo $grad; ?>. I created this website as part of a class project.
                 I'm happy to answer any questions aspiring devs may have, and can be reached at <?php echo $email; ?> You can find out
                 more about my personal experience below, or even browse our <a href="articles.php">articles</a> and see what I've 
                 written.</span></div>
            <div><img class="uk-align-right" src="<?php echo $photo; ?>"></div>
        </div>
    </div>

    <div class="uk-box-shadow-large" style="margin-top: 2%;">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h1 class="uk-heading-line uk-text-center white-text">
                <p>My Courses</p>
            </h1>

            <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slider>
                <ul class="uk-slider-items uk-container white-text" style="padding: 2%;">
                    <?php
                            //Gets college info
                            $courses = simplexml_load_file("xml/Courses.xml");    
                            foreach($courses->Class as $class) {
                                $crn = $class->CRN;
                                $courseName = $class->Name;
                                $term = $class->Semester;
                                $prof = $class->Professor;
                                $courseDescription = $class->Description;

                                include("course.php");
                            }
                            ?>
                </ul>

                <a style="color: white;" class="uk-position-center-left uk-position-small uk-hidden-hover" href="#"
                    uk-slidenav-previous uk-slider-item="previous"></a>
                <a style="color: white;" class="uk-position-center-right uk-position-small uk-hidden-hover" href="#"
                    uk-slidenav-next uk-slider-item="next"></a>
            </div>
        </div>
    </div>
</main>