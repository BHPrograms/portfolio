<head>
    <title>Personal Experience</title>
    <script src="js/UIKit/slider.js"></script>
</head>



<main class="uk-container">
    <div class="uk-text-center" style="margin-top: 2%;">

        <div>
            <h1>The Development Industry</h1>
        </div>
        <div class="uk-flex uk-flex-center">
            <div class="uk-width-1-2">
                <p>Application Software Development is a field with many different prospects. Information on the
                    careers, the industry, and even courses required can be found below.</p>
            </div>
        </div>
    </div>

    <div class="uk-box-shadow-large">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h2 class="uk-heading-line uk-text-center white-text">
                <p>Career Options</p>
            </h2>
        </div>
        <?php
            //Gets college info
            $majors = simplexml_load_file("xml/CareerInfo.xml");
            $majorName = $majors->Major->Name;
            $majorDesc = $majors->Major->Description;  
        ?>

        <h2 class="uk-heading-line uk-text-center">
            <p><?php echo $majorName; ?></p>
        </h2>
        <div class="uk-align-center" style="width: 50%;">
            <p><?php echo $majorDesc ?></p>
        </div>

        <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slider>
            <ul class="uk-slider-items uk-container" style="padding: 2%;">
                <?php
                        foreach($majors->Major->Careers->Career as $career) {
                            $careerName = $career->Name;
                            $careerDesc = $career->Description; 
                            include("career.php");
                        }
                    ?>
            </ul>

            <a style="color: black;" class="uk-position-center-left uk-position-small uk-hidden-hover" href="#"
                uk-slidenav-previous uk-slider-item="previous"></a>
            <a style="color: black;" class="uk-position-center-right uk-position-small uk-hidden-hover" href="#"
                uk-slidenav-next uk-slider-item="next"></a>
        </div>
    </div>

    <?php
        include_once("model/php/articles.php");
        displayIndustryArticles();
    ?>

    <div class="uk-box-shadow-large" style="margin-top: 2%;">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h2 class="uk-heading-line uk-text-center white-text">
                <p>Relevant Courses</p>
            </h2>

            <div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slider>
                <ul class="uk-slider-items uk-container white-text" style="padding: 2%;">
                    <?php
                            //Gets college info
                            $courses = simplexml_load_file("xml/Courses.xml");    
                            foreach($courses->Class as $class) {
                                $crn = $class->CRN;
                                $courseName = $class->Name;
                                $term = $class->Semester;
                                $prof = $class->Professor;
                                $courseDescription = $class->Description;

                                include("course.php");
                            }
                            ?>
                </ul>

                <a style="color: white;" class="uk-position-center-left uk-position-small uk-hidden-hover" href="#"
                    uk-slidenav-previous uk-slider-item="previous"></a>
                <a style="color: white;" class="uk-position-center-right uk-position-small uk-hidden-hover" href="#"
                    uk-slidenav-next uk-slider-item="next"></a>
            </div>
        </div>
    </div>
</main>