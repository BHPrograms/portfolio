<head>
    <title>The College Experience</title>
    <script src="js/UIKit/slider.js"></script>
</head>



<main class="uk-container">

    <div class="uk-box-shadow-large">
        <div class="purple" style="padding-top: 1%; padding-bottom: 1%; margin-top: 5%; margin-bottom: 1%;">
            <h1 class="uk-heading-line uk-text-center white-text">
                <p>The College Experience</p>
            </h1>
        </div>

        <div style="padding: 1%;">
            <!--Gets basic info from XML and formats it-->
            <?php include("model/php/basic_college_info.php")?>
            <p class="uk-text-break uk-text-large">There are a wide variety of courses required in order to
                study Applications Software Development. The below is a list of courses that one student,
                <?php echo $name; ?> supplied. These courses are taken from <?php echo $college_count ?>
                colleges, <?php echo $college_name_list?>.
            </p>
        </div>

        <?php include("views/colleges.php"); ?>
    </div>

</main>