<?php
    include("views/header.php");

    if (!isset($_SESSION["userProfile"])) {
        include("views/errors/signInReq.php");
    } else {
        /*Used to evaluate if article exists,
        and retrieve content if so*/
        include_once("model/php/articles.php");
        

        $articleID = "";
        if (isset($_GET['article'])) {
            $articleID = $_GET['article'];
        }

        /*Any nonnumeric article ID will convert to
        0 and retrieve article 0. Instead, it will
        stay false, and display 404. False is also 
        returned when article doesn't exist*/
        $article = false;
        if (is_numeric($articleID)) {
            $article = retrieveArticle($articleID);
        }
        
        if ($article != false) {
            //Evals not signed in, signed in but no profile, signed in with profile
            if (!isset($_SESSION["userProfile"])) {
                //User isn't signed in, display instead of edit
                include("views/articles/article.php");
            } else if ($_SESSION["userProfile"]["exists"] == false) {
                //User doesn't have a profile, they didn't write this article, display
                include("views/articles/article.php");
            } else {
                if ($_SESSION["userProfile"]["profileID"] == $article["profileID"]) {
                    //User is signed in, has profile, profiles match
                    include("views/articles/edit_article.php");
                } else {
                    //User simply isn't the author
                    include("views/articles/article.php");
                }
            }

        } else {
            $contentType = "Article";
            $redirect = "articles.php";
            $redirect_text = "our articles";
            include("views/errors/404.php");
        }

    }

    include("views/footer.php");
?>