/*Formats based on login state, sends info server side*/
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        username = user.email;
        formatSignedIn();
        setUserProfile(username);
    } else {
        username = "";
        formatSignedOut();
        logoutSession();
    }
});

/*Used to create server session*/
function setUserProfile(username) {
    var formData = new FormData();
    formData.append("action", "setProfile");
    formData.append("profileID", $.MD5(username));

    $.ajax({
        url: 'model/php/profiles.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(responseStr) {}
    });
}

/*Used to end server session*/
function logoutSession() {
    var formData = new FormData();
    formData.append("action", "logout");


    $.ajax({
        url: 'model/php/profiles.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(responseStr) { console.log(responseStr); }
    });
}

/*Basic formatting for auth controls*/
function formatSignedIn() {
    $(".signedOutReq").hide();
    $(".signedInReq").show();
    UIkit.dropdown($("#auth-dropdown")).hide();
    $("body").css("display", "block");
}

function formatSignedOut() {
    $(".signedOutReq").show();
    $(".signedInReq").hide();
    UIkit.dropdown($("#auth-dropdown")).hide();
    $("body").css("display", "block");
}