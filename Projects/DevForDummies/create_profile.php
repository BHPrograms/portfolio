<?php
    /*Used to evaluate if article exists,
    and retrieve content if so*/
    include_once("model/php/profiles.php");
    
    include("views/header.php");

    /*PHP Header redirection is insufficient barring a redesign. Auth scripts and
    observers loaded from Header must be ran before username can be retrieved, which
    is required info for the session variables. These auth scripts on in JS, and
    the init state loads AFTER document.ready()*/
    if (isset($_SESSION["userProfile"]) && $_SESSION["userProfile"]["exists"]) {
        echo '<script type="text/javascript"> window.location.href = "' .
            "profile.php?profileID=" . $_SESSION["userProfile"]["profileID"] . '"; </script>';
    } else if (!isset($_SESSION["userProfile"])) {
        include("views/errors/signInReq.php");
    } else {
        include("views/profile/create_profile.php");
    }
    include("views/footer.php");
?>