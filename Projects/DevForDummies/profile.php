<?php
    /*Used to evaluate if article exists,
    and retrieve content if so*/
    include_once("model/php/profiles.php");
    
    include("views/header.php");
    //False by default and if profile not found
    $profile = false;
    $profileID = "";
    if (isset($_GET['profileID'])) {
        $profileID = $_GET['profileID'];
        $profile = retrieveProfile($profileID);
    }
    
    if ($profile != false) {
        
        
        //The ProfileID for myself in the DB, no other user can have this username as it isn't email based
        if ($profileID == "04372a676523fb5dfaded1ef3b99423a") {
            include("views/profile/xml_profile.php");
            include("views/profile/user_articles.php");
        } else if (isset($_GET['profileID']) && $profile != false) {
            include("views/profile/profile.php");
            include("views/profile/user_articles.php");

            if (isset($_SESSION["userProfile"]) && $profileID == $_SESSION["userProfile"]["profileID"]) {
                include("views/profile/user_courses.php");
            }
        }
    } else {
        if (isset($_SESSION["userProfile"]) && $_SESSION["userProfile"]["exists"] == false && $profileID == $_SESSION["userProfile"]["profileID"]) {
            echo '<script type="text/javascript"> window.location.href = "create_profile.php"; </script>';
        } else {
            /*No current plans for searching profiles,
            or displaying all profiles, this redirect
            to articles is intentional.*/
            $contentType = "Profile";
            $redirect = "articles.php";
            $redirect_text = "our articles";
            include("views/errors/404.php");
        }
    }

    include("views/footer.php");
?>