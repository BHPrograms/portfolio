<?php
    include("views/header.php");


    //Evals not signed in, signed in but no profile, signed in with profile
    if (!isset($_SESSION["userProfile"])) {
        include("views/errors/signInReq.php");
    } else if ($_SESSION["userProfile"]["exists"] == false) {
        include("views/profile/create_profile.php");
    } else {
        include("views/articles/create_article.php");
    }

    include("views/footer.php");
?>