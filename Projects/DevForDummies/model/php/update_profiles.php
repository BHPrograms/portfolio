<?php
    if (isset($_POST['action']) ) {
        require_once("DBConfig.php");
        require_once("profiles.php");
        switch ($_POST['action']) {
            case 'create':
                create();
                break;
            case 'edit':
                edit();
                break;
            case 'delete':
                delete();
                break;
        }
    }
    
    function create() {
        global $conn;
        $responseStr = "";
        try {
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            //Allows a unique, non-identifying profileID
            $profileID = md5($username);
            $name = filter_input(INPUT_POST, 'author_name', FILTER_SANITIZE_STRING);
            $bio = filter_input(INPUT_POST, 'author_bio', FILTER_SANITIZE_STRING);

            if (isset(pathinfo($_FILES["author_thumbnail"]["name"])['extension'])) {
                $ext = pathinfo($_FILES["author_thumbnail"]["name"])['extension'];
                $filePath = "img//profiles//" . $profileID . "." . $ext;
            } else {
                $filePath = "img/defaults/profile_default.png";
            }

            $stmt =  $conn->prepare('INSERT INTO profile(profileID, username, name, bio, thumbnail)
                                    VALUES(:ID, :username, :name,:bio, :thumbnail);');
            $stmt->bindParam(':ID', $profileID);                    
            $stmt->bindParam(':username', $username);
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':bio', $bio);
            $stmt->bindParam(':thumbnail', $filePath);
        
            if ($stmt->execute()) {
                $responseStr = "Create success";
                move_uploaded_file($_FILES["author_thumbnail"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "//" . $filePath);
            } else {
                $responseStr = "Create failure";
            }
        } catch (Exception $e) {
            $responseStr = "Unspecified errror" . $e->getMessage();
        }

        echo $responseStr;
        exit;
    }

    function edit() {
        global $conn;
        $responseStr = "";
        try {
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            //Allows a unique, non-identifying profileID
            $profileID = md5($username);
            $name = filter_input(INPUT_POST, 'edit_author_name', FILTER_SANITIZE_STRING);
            $bio = filter_input(INPUT_POST, 'edit_author_bio', FILTER_SANITIZE_STRING);

            if (isset(pathinfo($_FILES["edit_profile_thumbnail"]["name"])['extension'])) {
                $ext = pathinfo($_FILES["edit_profile_thumbnail"]["name"])['extension'];
                $filePath = "img//profiles//" . $profileID . "." . $ext;
            } else {
                $filePath = retrieveProfile($profileID)["thumbnail"];
                $responseStr = "Invalid photo";
            }

            $stmt =  $conn->prepare('UPDATE profile SET name = :name, bio = :bio, thumbnail = :thumbnail WHERE profileID = :profileID;');                
            $stmt->bindParam(':name', $name);
            $stmt->bindParam(':bio', $bio);
            $stmt->bindParam(':thumbnail', $filePath);
            $stmt->bindParam(':profileID', $profileID);    
        
            if ($stmt->execute()) {
                $responseStr = "Edit success";
                if (isset($_FILES["edit_profile_thumbnail"]["tmp_name"])) {
                    move_uploaded_file($_FILES["edit_profile_thumbnail"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "/" . $filePath);
                }
            } else {
                $responseStr = "Edit failure";
            }
        } catch (Exception $e) {
            $responseStr = "Unspecified errror" . $e->getMessage();
        }

        echo $responseStr;
        exit;
    }

    function delete() {
        global $conn;
        $responseStr = "";
        try {
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            //Allows a unique, non-identifying profileID
            $profileID = md5($username);

            //Used to remove unneeded file
            $filePath = retrieveProfile($profileID)["thumbnail"];

            $stmt =  $conn->prepare('DELETE FROM profile WHERE profileID = :profileID; DELETE FROM AppSoftArticle WHERE author = :author');                
            $stmt->bindParam(':profileID', $profileID);    
            $stmt->bindParam(':author', $username);   
        
            if ($stmt->execute()) {
                $responseStr = "Delete success";
                if ($filePath != "img/defaults/profile_default.png") {
                    unlink( $_SERVER['DOCUMENT_ROOT'] . "//" . $filePath);
                }
            } else {
                $responseStr = "Delete failure";
            }
        } catch (Exception $e) {
            $responseStr = "Unspecified errror" . $e->getMessage();
        }

        echo $responseStr;
        exit;
    }
?>