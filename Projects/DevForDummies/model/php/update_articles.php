<?php
    if (isset($_POST['action']) ) {
        require_once("DBConfig.php");
        require_once("articles.php");
        switch ($_POST['action']) {
            case 'create':
                create();
                break;
            case 'edit':
                edit();
                break;
            case 'delete':
                delete();
                break;
        }
    }
    
    function create() {
        global $conn;
        global $articles;
        $responseStr = "";
        $responseID = "";
        try {
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'article_title', FILTER_SANITIZE_STRING);
            $content = filter_input(INPUT_POST, 'article_content', FILTER_SANITIZE_STRING);

            $filePath = "img/defaults/article_default.png";
            if (isset(pathinfo($_FILES["article_thumbnail"]["name"])['extension'])) {
                //Gets original extension type for thumbnail
                $ext = pathinfo($_FILES["article_thumbnail"]["name"])['extension'];

                /*Path used in server. Author and Title are used, matches UNIQUE
                constraint in DB*/
                $filePath = "img//articles//" . $username . "_" . $title . "." . $ext;
            }
            

            $stmt =  $conn->prepare('INSERT INTO article(title, author, content, thumbnail)
                                    VALUES(:title, :author,:content, :thumbnail);');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':author', $username);
            $stmt->bindParam(':content', $content);
            $stmt->bindParam(':thumbnail', $filePath);
        
            if ($stmt->execute()) {
                $responseStr = "Create success";
                $responseID = $conn->lastInsertId();
                move_uploaded_file($_FILES["article_thumbnail"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "//" . $filePath);
            } else {
                $responseStr = "Create failure";
            }

            
        } catch (Exception $e) {
            $responseStr = $e->getMessage();
        }

        $responseArr = array($responseStr, $responseID);
        echo json_encode($responseArr);
        exit;
    }

    function edit() {
        global $conn;
        $responseStr = "";
        $responseID = "";
        try {
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $title = filter_input(INPUT_POST, 'edit_article_title', FILTER_SANITIZE_STRING);
            $content = filter_input(INPUT_POST, 'edit_article_content', FILTER_SANITIZE_STRING);
            $ID = filter_input(INPUT_POST, 'ID', FILTER_SANITIZE_STRING);

            if (isset(pathinfo($_FILES["edit_article_thumbnail"]["name"])['extension'])) {
                //Gets original extension type for thumbnail
                $ext = pathinfo($_FILES["edit_article_thumbnail"]["name"])['extension'];

                /*Path used in server. Author and Title are used, matches UNIQUE
                constraint in DB*/
                $filePath = "img//articles//" . $username . "_" . $title . "." . $ext;
            } else {
                $filePath = retrieveArticle($ID)["articleThumbnail"];
            }

            

            $stmt =  $conn->prepare('UPDATE article SET title = :title, content = :content, thumbnail = :thumbnail WHERE ID = :ID AND author = :author');
            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':content', $content);
            $stmt->bindParam(':thumbnail', $filePath);
            $stmt->bindParam(':ID', $ID);
            $stmt->bindParam(':author', $username);
        
            if ($stmt->execute()) {
                $responseStr = "Edit success";
                $responseID = $ID;
                move_uploaded_file($_FILES["edit_article_thumbnail"]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . "//" . $filePath);
            } else {
                $responseStr = "Edit failure";
            }

            
        } catch (Exception $e) {
            $responseStr = $e->getMessage();
        }

        $responseArr = array($responseStr, $responseID);
        echo json_encode($responseArr);
        exit;
    }

    function delete() {
        global $conn;
        $responseStr = "";
        try {
            
            //Sanitized inputs
            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $ID = filter_input(INPUT_POST, 'ID', FILTER_SANITIZE_STRING);

            //Used to delete unneeded file
            $filePath = retrieveArticle($ID)["articleThumbnail"];

            $stmt =  $conn->prepare('DELETE FROM AppSoftArticle WHERE ID = :ID AND author = :author');                
            $stmt->bindParam(':ID', $ID);    
            $stmt->bindParam(':author', $username);   
        
            if ($stmt->execute()) {
                $responseStr = "Delete success";
                if ($filePath != "img/defaults/article_default.png") {
                    unlink( $_SERVER['DOCUMENT_ROOT'] . "//" . $filePath);
                }
            } else {
                $responseStr = "Delete failure";
            }
        } catch (Exception $e) {
            $responseStr = "Unspecified errror" . $e->getMessage();
        }

        $responseArr = array($responseStr);
        echo json_encode($responseArr);
        exit;
    }

?>