<?php
    require_once("DBConfig.php");

    function setProfile() {
        $userProfile = retrieveProfile($_POST['profileID']);
        if ($userProfile == false) {
            $_SESSION["userProfile"] = array("profileID"=>$_POST['profileID'], "exists"=>false);
        } else {
            $userProfile["exists"] = true;
            $_SESSION["userProfile"] = $userProfile;
        }
    }

    function logout() {
        session_destroy();
    }

    function retrieveProfile($profileID) {
        global $conn;

        $stmt = $conn->prepare('SELECT profileID, name, bio, thumbnail FROM profile WHERE profileID = :ID');
        $stmt->bindParam(":ID", $profileID);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $profileData = $stmt->fetch(PDO::FETCH_ASSOC);
            return $profileData;
        } else {
            return false;
        }
    }

    if (isset($_POST['action']) ) {
        switch ($_POST['action']) {
            case 'setProfile':
                    session_start();
                    setProfile();
                break;
            case 'logout':
                session_start();
                logout();
                break;
        }
    }
?>