<?php 
    //Gets name from XML
    $students = simplexml_load_file("xml/StudentInfo.xml");
    $student = $students->Student;

    $name = $student->Name;
    $photo = $student->Photo;
    $email = $student->Email;
    $uni = $student->University;
    $dept = $student->Department;
    $major = $student->Major;
    $level = $student->Level;
    $standing = $student->Standing;
    $grad = $student->Graduation;
    $address = $student->Address;
?>