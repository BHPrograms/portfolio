<?php
    function addCourse() {
        //Retrieves user info
        $courseName = filter_input(INPUT_POST, 'course', FILTER_SANITIZE_STRING);
        $crn = filter_input(INPUT_POST, 'crn', FILTER_SANITIZE_STRING);
        $semester = filter_input(INPUT_POST, 'semester', FILTER_SANITIZE_STRING);
        $prof = filter_input(INPUT_POST, 'prof', FILTER_SANITIZE_STRING);
        $desc = filter_input(INPUT_POST, 'desc', FILTER_SANITIZE_STRING);

        //Loads XML file
        $xml = simplexml_load_file($_SERVER["DOCUMENT_ROOT"] . "/xml/UserCourses.xml");

        //Adds child
        $courses = $xml->Courses;
        $course = $courses->addChild("Class");
        $course->addChild("Name", $courseName);
        $course->addChild("CRN", $crn);
        $course->addChild("Semester", $semester);
        $course->addChild("Professor", $prof);
        $course->addChild("Description", $desc);


        //Formats file
        $xml->formatOutput = true;
        $xml->asXML($_SERVER["DOCUMENT_ROOT"] . "/xml/UserCourses.xml");

        echo "Course added successfuly";
    }

    function delCourse() {
        //Loads XML file
        $xml = simplexml_load_file($_SERVER["DOCUMENT_ROOT"] . "/xml/UserCourses.xml");

        //Finds first course with matching name, deletes
        foreach($xml->Courses->Class as $class)
        {
            if($class->Name == $_POST["course"]) {
                unset($class[0]);
                break;
            }
        }

        //Formats file
        $xml->formatOutput = true;
        $xml->asXML($_SERVER["DOCUMENT_ROOT"] . "/xml/UserCourses.xml");

        echo "Course deleted successfuly";
    }

    if (isset($_POST["action"])) {
        if ($_POST["action"] == "add") {
            addCourse();
        } else if ($_POST["action"] == "delete") {
            delCourse();
        }
    } else {
        echo "An unspecified error has occured";
    }

?>