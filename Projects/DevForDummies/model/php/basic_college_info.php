<?php 
                    //Gets name from XML
                    $students = simplexml_load_file("xml/StudentInfo.xml");
                    $name = $students->Student->Name;

                    //Gets college info
                    $colleges = simplexml_load_file("xml/CoursesByCollege.xml");
                    $college_count = 0;
                    $college_names = [];

                    //Creates an array of college names
                    foreach($colleges->College as $college) {
                        array_push($college_names, $college->Name);
                        $college_count++;
                    }
                    
                    //Formats college names into a list
                    $college_name_list = "";
                    for ($i = 0; $i < count($college_names); $i++) {
                        //Add appropriate separator
                        if ($i == (count($college_names) - 1)) {
                            $college_name_list .= " and ";
                        } else if ($i > 0) {
                            $college_name_list .= ", ";
                        }

                        //Add college name
                        $college_name_list .= $college_names[$i]; 
                    }
?>