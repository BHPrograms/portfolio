/*Despite being labeled as an onAuthStateChanged observer,
this is the only reliable way to retrieve current user.*/
var username;
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        username = user.email;
    } else {
        username = "";
    }
});


function updateProfile(formData) {
    $.ajax({
        url: 'model/php/update_profiles.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(responseStr) {
            //Parses response to array, gets response string, updates articles
            if (responseStr == "Create success") {
                UIkit.notification("Profile created successfuly", { status: 'success' });
                //After a delay, redirect to profile
                setUserProfile(username);
                var profileID = $.MD5(username);
                setTimeout(function() {
                    window.location.replace("profile.php?profileID=" + profileID);
                }, 2000);
            } else if (responseStr == "Create failure") {
                UIkit.notification("Profile could not be created", { status: 'danger' });
            } else if (responseStr == "Edit success") {
                UIkit.notification("Profile updated successfuly", { status: 'success' });
                //After a delay, redirect to profile
                setUserProfile(username);
                var profileID = $.MD5(username);
                setTimeout(function() {
                    window.location.replace("profile.php?profileID=" + profileID);
                }, 2000);
            } else if (responseStr == "Edit failure") {
                UIkit.notification("Profile could not be updated", { status: 'danger' });
            } else if (responseStr == "Delete success") {
                UIkit.notification("Profile deleted successfuly", { status: 'success' });
                //After a delay, redirect to profile
                setTimeout(function() {
                    window.location.replace("index.php");
                }, 2000);
            } else if (responseStr == "Delete failure") {
                UIkit.notification("Profile could not be deleted", { status: 'danger' });
            } else {
                UIkit.notification("An unspecified error has occured", { status: 'danger' });
            }
        }
    });
}