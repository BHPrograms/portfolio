$(document).ready(function() {
    $("#btn_upload").click(function() {
        if (username) {
            if ($('#create_article_form').valid()) {
                var form = $('#create_article_form')[0];
                var formData = new FormData(form);
                formData.append("username", username);
                formData.append("action", "create");

                updateArticles(formData);
            }
        } else {
            UIkit.notification("Login to upload a file.", { status: 'danger' });
        }
    });
});