$(document).ready(function() {
    $('#btn_signin').click(function(event) {
        event.preventDefault();
        if ($('#signin_form').valid()) {

            //Retrieves input fields, hashes pass
            var email = $("#signin_user").val();
            var password = $("#signin_pass").val();
            password = $.MD5(password);

            /*Signs the user in, displays any error messages. Auth state automatically
            changed in the Firebase auth observer*/
            firebase.auth().signInWithEmailAndPassword(email, password)
                .then(function(firebaseUser) {
                    UIkit.notification("Signed in successfuly", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                }).catch(function(error) {
                    var errorCode = error.code;

                    if (errorCode == "auth/wrong-password") {
                        $("#signin_error").text("Incorrect username or password.");
                    } else if (errorCode == "auth/invalid-email") {
                        $("#signin_error").text("An account with that address does not exist.");
                    }

                });
        }
    });

    $('#btn_signup').click(function(event) {
        event.preventDefault();
        if ($('#signup_form').valid()) {

            //Retrieves input fields, hashes pass
            var email = $("#signup_user").val();
            var password = $("#signup_pass").val();

            /*Signs the user in, displays any error messages. Auth state automatically
            changed in the Firebase auth observer*/
            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
                var errorMessage = error.message;
                if (errorMessage == "The email address is already in use by another account.") {
                    $("#signup_error").text("This email address is already in use.")
                } else {
                    $("#signup_error").text("An unspecified error has occured. Please contact an administrator.")
                }
            });

        }
    });

    $('#li_logout').click(function(event) {
        firebase.auth().signOut().then(function() {
            UIkit.notification("Signed out successfuly", { status: 'success' });
            setTimeout(function() {
                location.reload();
            }, 2000);
        }, function(error) {
            alert("Unknown error occured: Could not logout")
        });
    });
});