/*Despite being labeled as an onAuthStateChanged observer,
this is the only reliable way to retrieve current user.*/
var username;
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        displayArticleEdit(username);
    } else {
        username = "";
    }
});

function displayArticleEdit(username) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const profileID = urlParams.get('profileID')
    if (profileID == $.MD5(username)) {
        $(".user-article-edit").show();
    }
}