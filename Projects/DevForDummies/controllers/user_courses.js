$(document).ready(function() {
    function updateCourses(formData) {
        $.ajax({
            url: 'model/php/user_courses.php',
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(responseStr) {
                if (responseStr == "Course added successfuly") {
                    UIkit.notification("Coursed added successfuly", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (responseStr == "Course deleted successfuly") {
                    UIkit.notification("Course deleted successfuly", { status: 'success' });
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if (responseStr == "An unspecified error has occured") {
                    UIkit.notification("An unspecified error has occured", { status: 'danger' });
                }
            }
        });
    }

    $("#add_course").click(function() {
        var form = $('#add-courses')[0];
        var formData = new FormData(form);
        formData.append("action", "add");
        updateCourses(formData);
    });

    $(".course_delete").click(function() {
        var formData = new FormData();
        formData.append("action", "delete");
        formData.append("course", $(this).val());
        updateCourses(formData);
    });
});