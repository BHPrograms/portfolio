$(document).ready(function() {
    $("#calculate_salary").click(function() {
        if (isNaN($("#salary_wage").val()) || isNaN($("#salary_hours").val())) {
            $("#salary_info").text("Please make sure wages and hours are numbers.");
        } else {
            var salary = $("#salary_wage").val() * $("#salary_hours").val() * 52;
            $("#salary_info").text("Your yearly salary would be: $" + salary.toFixed(2));
        }
    });
});