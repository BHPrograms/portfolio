/*Despite being labeled as an onAuthStateChanged observer,
this is the only reliable way to retrieve current user.*/
var username;
firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        displayEdit(username);
    } else {
        username = "";
    }
});

function displayEdit(username) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const profileID = urlParams.get('profileID')
    if (profileID == $.MD5(username)) {
        var redirect = "edit_profile.php?profileID=" + profileID;
        $("#profile_header").prepend('<a href="' + redirect + '"><span uk-icon="icon: cog; ratio: 3;" class="uk-text-middle"></span><a/>');
    }
}