$(document).ready(function() {
    $("#btn_update").click(function() {
        if (username) {
            if ($('#edit_article_form').valid()) {
                var form = $('#edit_article_form')[0];
                var formData = new FormData(form);
                formData.append("username", username);
                formData.append("action", "edit");

                //Gets articleID
                const queryString = window.location.search;
                const urlParams = new URLSearchParams(queryString);
                const articleID = urlParams.get('article');
                formData.append("ID", articleID);

                updateArticles(formData);
            }
        } else {
            UIkit.notification("Login to upload a file.", { status: 'danger' });
        }
    });

    $("#btn_delete_article").click(function() {
        if (username) {
            var formData = new FormData();
            //Gets articleID
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const articleID = urlParams.get('article');

            formData.append("ID", articleID);
            formData.append("username", username);
            formData.append("action", "delete");

            updateArticles(formData);
        } else {
            UIkit.notification("Login to upload a file.", { status: 'danger' });
        }
    });
});