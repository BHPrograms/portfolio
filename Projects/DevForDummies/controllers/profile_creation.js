$(document).ready(function() {
    $("#btn_submit").click(function() {
        if (username) {
            if ($('#create_profile_form').valid()) {
                var form = $('#create_profile_form')[0];
                var formData = new FormData(form);
                formData.append("username", username);
                formData.append("action", "create");
                updateProfile(formData);
            }
        } else {
            UIkit.notification("Login to create a profile.", { status: 'danger' });
        }
    });

    $("#author_thumbnail").change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#profile_photo').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });
});