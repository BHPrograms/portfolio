$(document).ready(function() {

    $("#btn_edit_profile").click(function() {
        if (username) {
            if ($('#edit_profile_form').valid()) {
                var form = $('#edit_profile_form')[0];
                var formData = new FormData(form);
                formData.append("username", username);
                formData.append("action", "edit");
                formData.append("currentThumbnail", $("#edit_profile_photo").attr('src'));

                updateProfile(formData);
            }
        } else {
            UIkit.notification("Login to upload a file.", { status: 'danger' });
        }
    });

    $("#btn_delete_profile").click(function() {
        if (username) {
            if ($('#edit_profile_form').valid()) {
                var formData = new FormData();
                formData.append("username", username);
                formData.append("action", "delete");
                updateProfile(formData);
            }
        } else {
            UIkit.notification("Login to upload a file.", { status: 'danger' });
        }
    });


    $("#edit_profile_thumbnail").change(function() {
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#edit_profile_photo').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);
        }
    });
});