$(document).ready(function() {
    $("#btn-dark").click(function() {
        $(".purple").removeClass("purple").addClass("dark");
        $("#btn-dark").hide();
        $("#btn-light").show();
    });

    $("#btn-light").click(function() {
        console.log("running");
        $(".dark").removeClass("dark").addClass("purple");
        $("#btn-dark").show();
        $("#btn-light").hide();
    });
});