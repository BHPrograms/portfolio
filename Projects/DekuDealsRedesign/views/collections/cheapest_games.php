<h1 class="white-text uk-text-left">Cheapest Games</h1>
<p class="white-text">Cheapest Switch games the internet has to offer.</p>

<div class="uk-flex uk-flex-center">
<?php
    include_once("model/php/games.php");

    $cheapestGames = retrieveCheapest(4);

    foreach($cheapestGames as $game) {
        include("views/collections/collection_slide.php");
    }

    $redirect = "";
    include("views/see_more.php");
?>
</div>