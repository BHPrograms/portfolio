<h1 class="white-text uk-text-left">Highly Rated</h1>
<p class="white-text">Check out the best rated games.</p>

<div class="uk-flex uk-flex-center">
<?php
    include_once("model/php/games.php");

    $cheapestGames = retrieveHighlyRated(4);

    foreach($cheapestGames as $game) {
        include("views/collections/collection_slide.php");
    }

    $redirect = "";
    include("views/see_more.php");
?>
</div>