<div class="uk-card primary-bk uk-width-1-5" style="margin: 10px;">
    <a href="gameinfo.php?gameID=<?php echo $game["id"]; ?>" style="text-decoration: none;">
        <div class="uk-card-media-top uk-height-medium uk-flex uk-flex-center">
            <img src="<?php echo $game["thumbnail"]; ?>" style="max-height: 100%; max-width: 100%;">
        </div>
        <div class="uk-card-body truncate accent-text" style="padding-top: 5px;">
            <span class="accent-text uk-text-bolder uk-text-center"><?php echo $game["name"]; ?></span>
            <br>


            <div class="uk-flex uk-flex-center" style="margin: 0; padding: 0;">
            <?php 

                if ($game["deal"] < $game["retailprice"]) {
                    $discount =  ceil((1 - $game["deal"]/$game["retailprice"]) * 100);
                    echo  '<span class="markdown-alert uk-text-nowrap" style="margin: 2px;">-' . $discount .'%</span>';
                }

                if ($game["deal"] == $game["lowestprice"]) {
                    echo '<span class="price-alert uk-text-nowrap" style="margin: 2px;">Lowest price ever</span>';
                }

                if ($game["deal"] >= $game["retailprice"] && $game["deal"] != $game["lowestprice"]) {
                    //Empty space equivalent to the above spans
                    echo '<div style="margin: 2px;"><br></div>';
                }
             ?>
             </div>
            
             <div class="uk-flex uk-flex-center" style="margin: 0; padding: 0;">
                <?php 
                    if ($game["deal"] < $game["retailprice"]) {
                        echo '<span class="muted-text uk-text-center uk-text-nowrap" style="margin: 2px;">' . $game["retailprice"] .'</span>
                        <span class="white-text uk-text-bolder uk-text-center uk-text-nowrap" style="margin: 2px;">' . $game["deal"] .'</span>';

                    } else {
                        echo '<span class="white-text uk-text-bolder uk-text-center uk-text-nowrap" style="margin: 2px;">' . $game["retailprice"] .'</span>';
                    }
                ?>
            </div>
        </div>
    </a>
</div>