<!DOCTYPE html>
<html>

<head>
    <title>DekuDeals Redesign</title>
    <!-- UIkit CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/css/uikit.min.css" />

    <!-- UIkit JS -->
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/uikit@3.3.3/dist/js/uikit-icons.min.js"></script>

    <!--JQuery CDN-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!--General color palette-->
    <link href="css/palette.css" rel="stylesheet">

    <!--Fixes small issues caused otherwhere-->
    <link href="css/helpers.css" rel="stylesheet">

    <meta charset="utf-8" />
</head>

<body>
    <div class="page-wrapper primary-bk">
        <nav class="uk-navbar-container accent-bk uk-box-shadow-large" uk-navbar style="padding: 5px;">
            <div class="uk-navbar-center uk-flex uk-flex-between uk-width-2-3@l">
                <div class="uk-flex uk-flex-middle">
                    <div><a href="index.php"><img src="img/brand_logos/long_logo.png"
                                style="height: 32px; width: auto;"></img></a></div>
                    <div>
                        <p class="uk-text-lead white-text" style="margin: 0px; margin-left: 10px; padding-top: 5px;">
                            Unofficial Redesign</p>
                    </div>
                </div>

                <ul class="uk-navbar-nav">
                    <li class="uk-flex uk-flex-middle">
                        <form class="uk-search uk-search-default">
                            <span uk-search-icon></span>
                            <input class="uk-search-input uk-border-pill black-text" type="search"
                                placeholder="Search..." style="background-color: white;" uk-tooltip="Dummy search">
                        </form>
                    </li>
                    <li style="margin: 2px;">
                        <a class="muted-text" uk-tooltip="Dummy link">Login</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="secondary-bk" style="padding: 5px;">
            <div class="uk-navbar-center uk-flex uk-flex-left uk-flex-middle uk-width-2-3@l">
                <div style="margin-right: 10px;"><a class="background-text" uk-tooltip="Dummy link">Games</a></div>
                <div style="margin-right: 10px;"><a class="background-text" uk-tooltip="Dummy link">Hardware</a></div>
                <div style="margin-right: 10px;"><a class="background-text" uk-tooltip="Dummy link">DLC</a></div>
                <div style="margin-right: 10px;"><a class="background-text" uk-tooltip="Dummy link">Gift Cards</a></div>
            </div>
        </div>