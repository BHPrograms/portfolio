            <!--Total Black didn't sit with me right, and gradient messed with the flat design, so I chose a secondary
            background that's a bit darker-->
            <footer class="secondary-bk" style="margin-top: 2%;">
                <p class="uk-text-center">DekuDeals is made by <a uk-tooltip="Dummy link">Michael Fairley</a> in Austin, TX.</p>
                <div class="uk-flex uk-flex-center">
                    <div class="uk-width-2-3@l uk-flex uk-flex-between" style="padding-left: 10px; padding-right: 10px;">
                        <div class="uk-width-1-3">
                            <p class="uk-text-lead white-text" style="margin: 0;">Contact Us</p>
                            <div class="truncate"><a href="mailto:michael@dekudeals.com" uk-tooltip="Original site's owner">michael@dekudeals.com</a></div>
                            <div class="truncate"><a href="https://twitter.com/dekudeals" uk-tooltip="Original site's owner">Twitter @dekudeals</a></div>
                        </div>
                        <div class="uk-width-1-3">
                             <p class="uk-text-lead white-text" style="margin: 0;">Get Help</p>
                            <div><a uk-tooltip="Dummy link">About & FAQ</a></div>
                            <div><a uk-tooltip="Dummy link">Feature Requests</a></div>
                        </div>
                        <div class="uk-width-1-3">
                            <h3 class="white-text truncate">Support the site</h3>
                            <div>
                                <a style="margin: 5px;" uk-tooltip="Dummy button"><img src="img/donation/patron.png" style="height: 32px;"></a>
                                <!--Image is ever so slightly shorter than Patron's, adjusted by the pixel-->
                                <a style="margin: 5px;" uk-tooltip="Dummy button"><img src="img/donation/paypal.png" style="height: 38px;"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-flex uk-flex-center" style="padding: 10px;">
                    <hr class="uk-width-2-3@l">
                </div>
                <div class="uk-flex uk-flex-center">
                    <div class="uk-width-2-3@l uk-flex uk-flex-between" style="padding-left: 10px; padding-right: 10px;">
                        <div class="uk-width-1-3">
                            <p>This site uses affiliate links and earns a commission from certain links. This does not affect your purchases or the price you may pay.</p>
                        </div>
                        <div class="uk-width-1-3">
                        </div>
                        <div class="uk-width-1-3">
                            <div class="truncate" uk-tooltip="Dummy link"><a>Privacy Policy</a></div>
                            <div class="truncate" uk-tooltip="Dummy link"><a>Do Not Sell My Personal Information</a></div>
                        </div>
                    <div>
                </div>
            </footer>
        </div>
    </body>
</html>

