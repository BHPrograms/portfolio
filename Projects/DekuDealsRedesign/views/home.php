<div class="uk-flex uk-flex-center" style="margin-top: 25px; margin: 2%;">
    <div class="uk-width-2-3@l">
        <div class="uk-flex uk-flex-center uk-flex-middle uk-height-match">
            <h1 class="white-text" style="margin: 0; margin-right: 10px; padding: 0;"> Welcome to</h1>
            <img src="img/brand_logos/long_logo.png" style="height: 50px; width: auto;">
        </div>
        <div class="uk-flex uk-flex-center" style="margin-top: 10px;">
            <div style="width: 50%;">
                <p class="white-text uk-text-center">DekuDeals tracks the prices of Nintendo Switch games across the internet, all to get you the best deal possible. This is an unofficial partial redesign, done for a Human Computer Interactions course, hosted with permission from Mr. Fairley. Only the home and about game pages have been redesigned. Pricing and listings are not updated, and are purely for demonstration.</p>
            </div>
        </div>
        <?php include("views/collections/cheapest_games.php"); ?>
        <?php include("views/collections/best_deals.php"); ?>
        <?php include("views/collections/highly_rated.php"); ?>
    </div>
</div>