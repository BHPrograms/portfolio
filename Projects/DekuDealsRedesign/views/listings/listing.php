<div class="uk-flex" style="margin-top: 20px; margin-bottom: 20px; max-height: 30px; max-height">
    <div class="uk-width-2-5" style="margin-right: 20px;">
        <img src="<?php echo $listing["thumbnail"]; ?>" style="max-height: 30px; width: auto;">
    </div>
    <div class="uk-width-1-5 uk-flex uk-flex-left" style="margin-right: 20px;">
        <span class="white-text"><?php echo $listing["listingtype"]; ?></span>
    </div>

    <div class="uk-width-2-5 uk-flex uk-flex-middle uk-flex-left">
        <div style="width: 75%;">

        <?php 
            $class = "";
            $label = "";
            if ($listing["price"] < $game["retailprice"]) { 
                $class = "light-accent-bk uk-flex uk-flex-center uk-flex-between"; 

                $discount =  ceil((1 - $listing["price"]/$game["retailprice"]) * 100);
                $label ='<span class="markdown-alert uk-text-left truncate" style="margin: 2px;">-' . $discount .'%</span>';

                $color = "light-accent-bk";
            } else { 
                $class = "accent-bk uk-flex uk-flex-right"; 
                $color = "accent-bk";
            } 
        ?>
        <a href="<?php echo $listing["listingurl"]; ?>" style="text-decoration: none;">
            <div class=" <?php echo $class; ?>" style="border-radius: 10px; padding: 10px;">
                <?php echo $label;?>
                <span class="white-text uk-text-bold uk-text-center truncate">$<?php echo $listing["price"]; ?></span>            
            </div>
        </a>

        <?php
            if ($listing["price"] == $game["lowestprice"]) {
                echo '<div class="uk-flex uk-flex-center"><span class="price-alert uk-text-nowrap" style="margin: 2px;">Lowest price ever</span></div>';
            }
        ?>
        </div>
    </div>
</div>