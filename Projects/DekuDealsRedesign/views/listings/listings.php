
<h2 class="white-text">Current prices</h2>
<hr>
<div class="uk-flex">
    <div class="uk-width-2-5">
        <p class="white-text uk-text-bold">Store</p>
    </div>
    <div class="uk-width-1-5">
        <p class="white-text uk-text-bold">Format</p>
    </div>
    <div class="uk-width-2-5">
        <p class="white-text uk-text-bold">Price</p>
    </div>
</div>
<?php
    $listings = retrieveListings($game["id"]);

    foreach($listings as $listing) {
        include("views/listings/listing.php");
    }
?>
