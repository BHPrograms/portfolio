            <!--Not used for purposes of this assignment, this is to be used in my personal portfolio.-->
            
            <!--Total Black didn't sit with me right, and gradient messed with the flat design, so I chose a secondary
            background that's a bit darker-->
            <footer class="secondary-bk uk-flex uk-flex-center" style="margin-top: 2%;">
                <div class="uk-width-2-3@l uk-flex uk-flex-wrap uk-flex-around" style="padding-left: 10px; padding-right: 10px;">
                    <div class="uk-width-1-2" style="padding: 10px;">
                        <h3 class="white-text truncate">BHPrograms</h3>
                        <ul class="uk-list">
                            <li class="white-text truncate"><a class="white-text" href="https://www.bhprogramsdevelopment.com">BHProgramsDevelopment.com</a>
                            </li>
                            <li class="white-text truncate"><a class="white-text" href="tel:6073013986">Phone: 607-301-3986</a></li>
                            <li class="white-text truncate"><a class="white-text" href="mailto:info@bhprogramsdevelopment.com">Email:
                                    Info@BHProgramsDevelopment.com</a></li>
                        </ul>
                    </div>
                    <div class="uk-width-1-2" style="padding: 10px;">
                        <h3 class="white-text truncate">About this site</h3>
                        <p class="white-text"> This site is a partial redesign of <a href="https://www.dekudeals.com/">DekuDeals.com</a> created for a school project. 
                        DekuDeals and all associated brand items are the sole property of <a href="https://www.dekudeals.com/about">Michael Fairley</a>.</p>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>