
<div class="uk-container uk-text-center" uk-height-viewport="offset-top: true; offset-bottom: true;">
    <div style="margin-top: 5%">
        <h1 class="white-text uk-heading-xlarge"><?php echo $contentType; ?> not found.</h1>
        <h2 class="white-text uk-heading-small">Why not check out <a href="<?php echo $redirect; ?>"> <?php echo $redirectText; ?></a> instead?</h1>
    </div>
</div>