<div class="uk-flex uk-flex-center" uk-height-viewport="offset-top: true; offset-bottom: true;">
    <div class="uk-width-2-3@l" style="margin: 15px;">
        <h1 class="white-text uk-heading-small"><?php echo $game["name"]; ?></h1>
        <div class="uk-flex uk-flex-left uk-grid-large" uk-grid>
            <div class="uk-width-1-2@m uk-width-1-3@l">
                <img src="<?php echo $game["thumbnail"]; ?>" class="uk-height-large"></img>
                <h2 class="white-text">Details</h2>
                <p class="white-text"><span class="uk-text-bold">Retail price: </span><span>$<?php echo $game["retailprice"];?></span></p>
                <p class="white-text"><span class="uk-text-bold">Released: </span><span><?php echo $game["released"];?></span></p>
                <p class="white-text"><span class="uk-text-bold">Genres: </span><span class="uk-text-break"><?php echo $game["genres"];?></span></p>
                <p class="white-text"><span class="uk-text-bold">Developer: </span><span><?php echo $game["developer"];?></span></p>
                <p class="white-text"><span class="uk-text-bold">Publisher: </span><span><?php echo $game["developer"];?></span></p>
                <p class="white-text"><span class="uk-text-bold">Metacritic user rating: 
                <?php
                    if ($game["metacritic"] >= 8) {
                        echo '</span><span class="good-rating">' . $game["metacritic"] . '</span></p>';
                    } else if ($game["metacritic"] >= 6) {
                        echo '</span><span class="medium-rating">' . $game["metacritic"] . '</span></p>';
                    } else {
                        echo '</span><span class="bad-rating">' . $game["metacritic"] . '</span></p>';
                    }
                ?>
                <p class="white-text"><span class="uk-text-bold">ESRB Rating: </span><span><?php echo $game["esrb"];?></span></p>
            </div>
            <div class="uk-width-1-2@m uk-width-2-3@l">
                <?php include("views/listings/listings.php"); ?>
                <h2 class="white-text">Description</h2>
                <p class="white-text"><?php echo $game["description"]; ?></p>

                <h2 class="white-text">Price History</h2>
                <p class="white-text"><span class="uk-text-bold">Lowest price: </span><span>$<?php echo $game["lowestprice"];?></span></p>
                <img src="<?php echo $game["pricehistory"]; ?>">
            </div>
        </div>
    </div>
</div>