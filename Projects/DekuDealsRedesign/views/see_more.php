<!--Positions See More at center of media, rather than center of card-->
<div class="uk-card primary-bk uk-width-1-5" style="margin: 10px;">
    <a href="<?php echo $redirect; ?>" style="text-decoration: none;">
        <div class="uk-card-media-top uk-height-medium uk-flex uk-flex-center uk-flex-middle">
            <a uk-tooltip="Dummy link"><p class="accent-text uk-text-bolder uk-text-center uk-text-middle">View the rest of the collection</p></a>
        </div>
    </a>
</div>