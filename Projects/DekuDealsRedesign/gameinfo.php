<?php
    include("views/header.php");
    include_once("model/php/games.php");

    $gameID = "";
    $game = false;
    if (isset($_GET['gameID'])) {
        $profileID = $_GET['gameID'];
        $game = retrieveGame($profileID);
    }

    if ($game != false) {
        include("views/game_info.php");
    } else {
        $contentType = "Game";
        $redirect = "index.php";
        $redirectText = "our deals";
        include("views/errors/404.php");
    }

   
    include("views/portfolio_footer.php");
?>