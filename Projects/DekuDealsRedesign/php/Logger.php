<?php

function logErr($errorName, $errorTag, $errorMessage) {
    $timestamp = date(' Y-m-d h-i-s'); 
    $filePath = $_SERVER['DOCUMENT_ROOT'] ."/logs/DBErrors/" . $errorName . $timestamp . ".txt"; 
    
    $errEntry = "/n" . $errorTag . " occured at " . $timestamp . "/n/t" . $errorMessage . "/n";
                        
    //Create new basic log file with info
    $errorLog = fopen($filePath, "wb");
    fwrite($errorLog, $errEntry);
}

?>