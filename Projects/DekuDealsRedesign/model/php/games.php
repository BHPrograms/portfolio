<?php
    require_once("DatabaseConfig.php");

    function retrieveCheapest($limit) {
        global $conn;

        $stmt = $conn->prepare('SELECT game.id, game.thumbnail, game.name, game.retailprice, game.lowestprice, cheapest.deal, cheapest.GameID
            FROM game, (SELECT GameID, MIN(Price) AS deal FROM listing GROUP BY GameID ORDER BY deal ASC LIMIT :limit) AS cheapest
            WHERE game.id = cheapest.GameID ORDER BY cheapest.deal ASC');
        $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $cheapest = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $cheapest;
        } else {
            return false;
        }
    }

    function retrieveBestDeals($limit) {
        global $conn;

        $stmt = $conn->prepare('SELECT game.id, game.thumbnail, game.name, game.retailprice, game.lowestprice, cheapest.deal, cheapest.GameID, 
        IF(game.retailprice > cheapest.deal, (1 - cheapest.deal/game.retailprice)*100, 0) AS markdown FROM game, (SELECT GameID, MIN(Price) AS deal FROM listing GROUP BY GameID 
       ORDER BY deal ASC) AS cheapest WHERE game.id = cheapest.GameID ORDER BY markdown DESC LIMIT :limit');
        $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $bestdeals = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $bestdeals;
        } else {
            return false;
        }
    }

    function retrieveHighlyRated($limit) {
        global $conn;

        $stmt = $conn->prepare('SELECT game.id, game.thumbnail, game.name, game.retailprice, game.lowestprice, cheapest.deal, cheapest.GameID
            FROM game, (SELECT GameID, MIN(Price) AS deal FROM listing GROUP BY GameID ORDER BY deal) AS cheapest
            WHERE game.id = cheapest.GameID ORDER BY metacritic DESC LIMIT :limit');
        $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $cheapest = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $cheapest;
        } else {
            return false;
        }
    }

    function retrieveGame($gameID) {
        global $conn;

        $stmt = $conn->prepare('SELECT id, thumbnail, name, description, genres, retailprice, lowestprice, pricehistory, released, developer, publisher, metacritic, esrb
            FROM game WHERE ID = :ID');
        $stmt->bindParam(":ID", $gameID, PDO::PARAM_INT);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $game = $stmt->fetch(PDO::FETCH_ASSOC);
            return $game;
        } else {
            return false;
        }
    }

    function retrieveListings($gameID) {
        global $conn;

        $stmt = $conn->prepare('SELECT listingtype, price, listingurl, name, thumbnail FROM listing, retailer WHERE GameID = :GameID AND 
            listing.retailerID = retailer.ID ORDER BY price');
        $stmt->bindParam(":GameID", $gameID, PDO::PARAM_INT);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $listings = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $listings;
        } else {
            return false;
        }
    }


?>